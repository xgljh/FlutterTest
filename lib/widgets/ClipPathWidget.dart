import 'package:flutter/material.dart';

class TriangleCliper extends CustomClipper<Path> {
  @override
  Path getClip(Size size) {
    var path = Path();
    path.moveTo(100, 100);
    path.lineTo(300, 100);
    path.lineTo(150, 300);
    path.close();
    return path;
  }

  @override
  bool shouldReclip(CustomClipper<Path> oldClipper) {
    return true;
  }
}

class ClipPathWidget extends StatefulWidget {
  @override
  State<StatefulWidget> createState() {
    return ClipPathWidgetState();
  }
}

class ClipPathWidgetState extends State<ClipPathWidget> {
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
        home: Scaffold(
      appBar: AppBar(
        title: Text("ClipPath"),
      ),
      body: Container(
        width: 200,
        height: 200,
        color: Colors.red,
        child: ClipPath(
          clipper: TriangleCliper(),
          child: Image.network(
            "http://img8.zol.com.cn/bbs/upload/23765/23764201.jpg",
            width: 100,
            height: 100,
            fit: BoxFit.cover,
          ),
        ),
      ),
    ));
  }
}
//
//const ClipPath({
//Key key,
//this.clipper,
//this.clipBehavior = Clip.antiAlias,
//Widget child,
//})
