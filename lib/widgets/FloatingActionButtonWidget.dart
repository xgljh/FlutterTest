import 'package:flutter/material.dart';

class FloatingActionButtonWidget extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      home: Scaffold(
          appBar: AppBar(
            title: Text("FloatingActionButton组件"),
          ),

          //centerDocked 底部中间
          //endDocked 底部右侧
          //centerFloat 中间偏上
          //endFloat 底部偏上
          //startTop 左侧顶部
          //endTop 右侧顶部
          floatingActionButtonLocation:
              FloatingActionButtonLocation.endTop,
          floatingActionButton: FloatingActionButton.extended(
            onPressed: () {},
            icon: Icon(Icons.home),
            label: Text("aaaaa"),
            shape: RoundedRectangleBorder(
                borderRadius: BorderRadius.all(Radius.circular(5))),
          )),
    );
  }
}

class FloatingActionButton1 extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return FloatingActionButton(
      onPressed: () {},
      child: Text("按钮"),
      tooltip: "常按添加",
      foregroundColor: Colors.green,
      backgroundColor: Colors.red,
      splashColor: Colors.orange,
      elevation: 5,
      highlightElevation: 20,
      disabledElevation: 10,
      shape: RoundedRectangleBorder(
          borderRadius: BorderRadius.all(Radius.circular(5))),
    );
  }
}
//const FloatingActionButton({
//Key key,
//this.child,
//this.tooltip,
//this.foregroundColor,
//this.backgroundColor,
//this.focusColor,
//this.hoverColor,
//this.splashColor,
//this.heroTag = const _DefaultHeroTag(),
//this.elevation,
//this.focusElevation,
//this.hoverElevation,
//this.highlightElevation,
//this.disabledElevation,
//@required this.onPressed,
//this.mini = false,
//this.shape,
//this.clipBehavior = Clip.none,
//this.focusNode,
//this.autofocus = false,
//this.materialTapTargetSize,
//this.isExtended = false,
//})
