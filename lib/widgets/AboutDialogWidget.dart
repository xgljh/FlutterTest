import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';

class AboutDialogWidget extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return CupertinoPageScaffold(
        child: Container(
      margin: EdgeInsets.all(50),
      child: CupertinoButton(
          child: Text("按钮"),
          onPressed: () {
            showDialog(
              context: context,
              builder: (context) {
                return AboutDialog(
                  applicationName: "呵呵哒",
                  applicationIcon: Icon(Icons.home),
                  applicationVersion: "v1.1.0",
                  applicationLegalese: "什么鬼东西",
                  children: <Widget>[
                    Text("一行"),
                    Text("一行"),
                    Text("一行"),
                    Text("又一行"),
                  ],
                );
              },
            );
          }),
    ));
  }
}
//const AboutDialog({
//Key key,
//this.applicationName,
//this.applicationVersion,
//this.applicationIcon,
//this.applicationLegalese,
//this.children,
//})
