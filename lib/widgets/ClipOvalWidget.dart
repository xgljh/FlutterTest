import 'package:flutter/material.dart';

class ClipOvalWidget extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
        home: Scaffold(
      appBar: AppBar(
        title: Text("ClipOval"),
      ),
      body: Container(
        width: 200,
        height: 200,
        child: ClipOval(
          child: Image.network(
            "http://img8.zol.com.cn/bbs/upload/23765/23764201.jpg",
            width: 100,
            height: 100,
            fit: BoxFit.cover,
          ),
        ),
      ),
    ));
  }
}
