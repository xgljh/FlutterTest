import 'package:flutter/material.dart';

class CircularProgressIndicatorWidget extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      home: Scaffold(
        appBar: AppBar(
          title: Text("CircularProgressIndicator组件"),
        ),
        body: Container(
          width: 100,
          height: 100,
          color: Colors.red,
          margin: EdgeInsets.all(20),
          child: CircularProgressIndicator(
              value: 0.6,
              strokeWidth: 3,
              backgroundColor: Colors.green,
              valueColor: AlwaysStoppedAnimation(Colors.blue)),
        ),
      ),
    );
  }
}

//const CircularProgressIndicator({
//Key key,
//double value,
//Color backgroundColor,
//Animation<Color> valueColor,
//this.strokeWidth = 4.0,
//String semanticsLabel,
//String semanticsValue,
//})
