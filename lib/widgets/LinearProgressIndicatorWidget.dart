import 'package:flutter/material.dart';

class LinearProgressIndicatorWidget extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      home: Scaffold(
        appBar: AppBar(
          title: Text("LinearProgressIndicatior组件"),
        ),
        body: ListView(
          children: <Widget>[
            Container(
              margin: EdgeInsets.only(top: 10),
              padding: EdgeInsets.all(20),
              child: LinearProgressIndicator(
                value: 0.5,
                backgroundColor: Colors.red,
                valueColor: AlwaysStoppedAnimation<Color>(Colors.green),
                semanticsLabel: "11",
                semanticsValue: "12",
              ),
            )
          ],
        ),
      ),
    );
  }
}

//const LinearProgressIndicator({
//Key key,
//double value,
//Color backgroundColor,背景色
//Animation<Color> valueColor,进度条颜色
//String semanticsLabel,
//String semanticsValue,
//})
