import 'package:flutter/material.dart';

class ColumnWidget extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      home: Scaffold(
        appBar: AppBar(
          title: Text("Column组件"),
        ),
        body: Column(
          children: <Widget>[
            Container(
              width: 60,
              height: 50,
              color: Colors.red,
            ),
            Container(
              width: 100,
              height: 100,
              color: Colors.green,
            ),
            Container(
              width: 100,
              height: 80,
              color: Colors.red,
            ),
            Container(
              width: 80,
              height: 100,
              color: Colors.blue,
            ),
          ],
          mainAxisAlignment: MainAxisAlignment.spaceBetween,

        ),
      ),
    );
  }
}

//Column({
//Key key,
//MainAxisAlignment mainAxisAlignment = MainAxisAlignment.start,
//MainAxisSize mainAxisSize = MainAxisSize.max,
//CrossAxisAlignment crossAxisAlignment = CrossAxisAlignment.center,
//TextDirection textDirection,
//VerticalDirection verticalDirection = VerticalDirection.down,
//TextBaseline textBaseline,
//List<Widget> children = const <Widget>[],
//})