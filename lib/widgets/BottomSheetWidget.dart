import 'package:flutter/material.dart';

class BottomSheetWidget extends StatefulWidget {
  @override
  State<StatefulWidget> createState() {
    return BottomSheetWidgetState();
  }
}

class BottomSheetWidgetState extends State<BottomSheetWidget> {
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      home: Scaffold(
          appBar: AppBar(
            title: Text("BottomSheet组件"),
          ),
          body: Container(
            child: RaisedButton(
              onPressed: () {
                showModalBottomSheet(
                  context: context,
                  backgroundColor: Colors.green,
                  shape: RoundedRectangleBorder(
                      borderRadius: BorderRadius.circular(10)),
                  enableDrag: false,
                  isDismissible: false,
                  builder: (BuildContext context) {
                    return new Container(
                        height: 300.0,
                        child: Column(
                          children: <Widget>[
                            RaisedButton(
                              onPressed: () {
                                Navigator.of(context).pop();
                              },
                              child: Text("点击关闭"),
                            ),
                          ],
                        ));
                  },
                ).then((val) {
                  print(val);
                });
              },
              child: Text("按钮"),
            ),
          )),
    );
  }
}

//Future<T> showModalBottomSheet<T>({
//@required BuildContext context,
//@required WidgetBuilder builder,
//Color backgroundColor,
//double elevation,
//ShapeBorder shape,
//Clip clipBehavior,
//Color barrierColor,
//bool isScrollControlled = false,
//bool useRootNavigator = false,
//bool isDismissible = true,
//bool enableDrag = true,
//})

class BottomSheet1 extends StatefulWidget {
  @override
  State<StatefulWidget> createState() {
    return BottomSheet1State();
  }
}

class BottomSheet1State extends State<BottomSheet1> {
  @override
  Widget build(BuildContext context) {
    return Builder(builder: (context) {
      return Container(
        child: RaisedButton(
          onPressed: () {
            print("!!!!!!!!1111111");
            Scaffold.of(context)
                .showBottomSheet<Null>((context) => Container(
                      height: 100,
                      color: Colors.red,
                      child: Text("BottomSheet组件"),
                    ))
                .closed
                .then((value) => {print("!!!!!!!!" + value)})
                .whenComplete(() => {
                      if (!mounted)
                        {print("!!!!!!!!已经销毁")}
                      else
                        {print("!!!!!!!!还在结构树中")}
                    });
          },
          child: Text("按钮"),
        ),
      );
    });
  }
}
