import 'package:flutter/material.dart';

class ScrollViewWidget extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      home: Scaffold(
//        appBar:,
        body: CustomScrollView(
          physics: BouncingScrollPhysics(), //ios风格的上拉出来空白
//        physics: ClampingScrollPhysics(),
          reverse: false,
          slivers: <Widget>[
            SliverAppBar(
              title: Text("scrollview"),
              centerTitle: true,
              leading: Icon(Icons.backspace),
              expandedHeight: 150,
              //AppBar是否固定在顶部
              pinned: true,

              floating: false,
              //
              backgroundColor: Colors.cyanAccent,//SliverAppBar背景色
              flexibleSpace: FlexibleSpaceBar(
                titlePadding: EdgeInsets.fromLTRB(50, 0, 0, 17),
                centerTitle: false,
                title: const Text('flexibleSpace'),
                background: Image.network(
                  "http://img8.zol.com.cn/bbs/upload/23765/23764201.jpg",
                  fit: BoxFit.cover,
                ),
              ),
            ),
            SliverPadding(
              padding: EdgeInsets.only(left: 10, right: 10),
              sliver: SliverList(
                  delegate: SliverChildListDelegate(<Widget>[
                Container(
                  child: Text("第1行"),
                  height: 300,
                  color: Colors.red,
                ),
                Container(
                  child: Text("第2行"),
                  height: 300,
                  color: Colors.green,
                ),
                Container(
                  child: Text("第3行"),
                  height: 300,
                  color: Colors.red,
                ),
                Container(
                  child: Text("第4行"),
                  height: 300,
                  color: Colors.green,
                ),
              ])),
            )
          ],
        ),
      ),
    );
  }
}

//const SliverAppBar({
//Key key,
//this.leading,
//this.automaticallyImplyLeading = true,
//this.title,
//this.actions,
//this.flexibleSpace,//SliverAppBar 底层的view
//this.bottom,
//this.elevation,
//this.forceElevated = false,
//this.backgroundColor,
//this.brightness,
//this.iconTheme,
//this.actionsIconTheme,
//this.textTheme,
//this.primary = true,
//this.centerTitle,
//this.excludeHeaderSemantics = false,
//this.titleSpacing = NavigationToolbar.kMiddleSpacing,
//this.expandedHeight, //展开后SliverAppBar的高度
//this.floating = false,  //false，当列表往下滑动时，会先将列表内容滚动到顶部，然后再将 SliverAppBar 滚动出来；true，当列表往下滑动时，会先将 SliverAppBar 滚动出来（与列表是否滚动到顶部无关），然后再继续列表的滑动
//this.pinned = false,//pinned = false 是默认行为，整个 AppBar 会随着列表的滚动完全消失在视野
//this.snap = false,
//this.stretch = false,
//this.stretchTriggerOffset = 100.0,
//this.onStretchTrigger,
//this.shape,
//})

//const CustomScrollView({
//Key key,
//Axis scrollDirection = Axis.vertical, //滚动放向
//bool reverse = false,//内容是否反着显示
//ScrollController controller, //控制滚动视图滚动到的位置
//bool primary,//是否是与父级关联的主滚动视图
//ScrollPhysics physics,  //滑动到底部回弹效果
//bool shrinkWrap = false,//根据正在查看的内容确定滚动视图的范围
//Key center,
//double anchor = 0.0,
//double cacheExtent,  // 视口在可见区域之前和之后有一个区域，用于缓存用户滚动时即将可见的项目。
//this.slivers = const <Widget>[],
//int semanticChildCount,
//DragStartBehavior dragStartBehavior = DragStartBehavior.start,
//})
