import 'package:flutter/material.dart';

class SnackBarWidget extends StatefulWidget {
  @override
  State<StatefulWidget> createState() {
    return SnackBarWidgetState();
  }
}

class SnackBarWidgetState extends State<SnackBarWidget> {
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      home: Scaffold(
        appBar: AppBar(
          title: Text("SnackBar组件"),
        ),
        body: Builder(builder: (context){
          return Container(
            child: RaisedButton(
              onPressed: () {
                Scaffold.of(context).showSnackBar(SnackBar(
                  content: Container(
                    child: Text("我是snackBar"),
                  ),
                  backgroundColor: Colors.grey,
                  elevation: 10,
                  action: SnackBarAction(label: "aaaaa", onPressed: (){
                    print("点击了");
                  }),
                  shape: RoundedRectangleBorder(
                      borderRadius: BorderRadius.circular(5)),
                ));
              },
              child: Text("按钮"),
            ),
          );
        }),
      ),
    );
  }
}

//const SnackBar({
//Key key,
//@required this.content,
//this.backgroundColor,
//this.elevation,
//this.shape,
//this.behavior,
//this.action,
//this.duration = _snackBarDisplayDuration,
//this.animation,
//this.onVisible,
//})
