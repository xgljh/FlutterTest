import 'package:flutter/material.dart';

class ClipRRectWidget extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
        home: Scaffold(
      appBar: AppBar(
        title: Text("ClipRRect"),
      ),
      body: Container(
        width: 200,
        height: 200,
        child: ClipRRect(
          borderRadius: BorderRadius.all(Radius.circular(10)),
          child: Image.network(
            "http://img8.zol.com.cn/bbs/upload/23765/23764201.jpg",
            width: 100,
            height: 100,
            fit: BoxFit.cover,
          ),
        ),
      ),
    ));
  }
}

//const ClipRRect({
//Key key,
//this.borderRadius = BorderRadius.zero,
//this.clipper,
//this.clipBehavior = Clip.antiAlias,
//Widget child,
//}) :