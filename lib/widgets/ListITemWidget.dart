
import 'package:flutter/material.dart';
import 'package:flutterapp/bean/ItemInfo.dart';


class ListItemWidget extends StatelessWidget {

  ListItemWidget(this.position, this.item, this.onItemClickListener);

  int position;
  ItemInfo item;
  OnItemClickListener onItemClickListener;

  @override
  Widget build(BuildContext context) {
    return InkWell(
      child: ListTile(
        title: Text(
          item.text,
        ),
      ),
      onTap: () {
        onItemClickListener(position);
      },
    );
  }
}

typedef OnItemClickListener = void Function(int position);