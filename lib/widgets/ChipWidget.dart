import 'package:flutter/material.dart';

class ChipWidget extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      home: Scaffold(
        appBar: AppBar(
          title: Text("chip组件"),
        ),
        body: Chip(
          label: Text("标签"),
          avatar: Icon(Icons.home),
          labelPadding: EdgeInsets.only(left: 0, right: 5),
          deleteIcon: Icon(Icons.delete),
          deleteIconColor: Colors.red,
          onDeleted: (){},
          backgroundColor: Colors.amberAccent,
          elevation: 10,
          shadowColor: Colors.blue,
          materialTapTargetSize: MaterialTapTargetSize.shrinkWrap,
        ),

      ),
    );
  }
}

//
//const Chip({
//Key key,
//this.avatar,
//@required this.label,
//this.labelStyle,
//this.labelPadding,
//this.deleteIcon,
//this.onDeleted,
//this.deleteIconColor,
//this.deleteButtonTooltipMessage,
//this.shape,
//this.clipBehavior = Clip.none,
//this.focusNode,
//this.autofocus = false,
//this.backgroundColor,
//this.padding,
//this.visualDensity,
//this.materialTapTargetSize,
//this.elevation,
//this.shadowColor,
//})
