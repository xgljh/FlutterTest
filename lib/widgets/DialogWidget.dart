import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';

class DialogWidgetTest extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      home: Scaffold(
        appBar: AppBar(
          title: Text("自定义dialog"),
        ),
        body: Container(
          margin: EdgeInsets.all(50),
          child: RaisedButton(
            onPressed: () {
              showDialog(
                  context: context,
                  builder: (context) {
                    return LoadingDialog(
                      message: "加载中...",
                    );
                  });
            },
            child: Text("按钮"),
          ),
        ),
      ),
    );
  }
}

class LoadingDialog extends Dialog {
  String message;

  LoadingDialog({Key key, @required this.message}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Material(
      type: MaterialType.transparency, //半透明背景
      child: Center(
        child: SizedBox(
          width: 100.0,
          height: 100.0,
          child: Container(
            decoration: ShapeDecoration(
              color: Colors.white,
              shape: RoundedRectangleBorder(
                borderRadius: BorderRadius.all(Radius.circular(10.0)),
              ),
            ),
            child: Column(
              mainAxisAlignment: MainAxisAlignment.center,
              crossAxisAlignment: CrossAxisAlignment.center,
              children: <Widget>[
                CupertinoActivityIndicator(animating: true, radius: 20),
                Padding(
                  padding: EdgeInsets.only(top: 20.0),
                  child: Text(
                    message,
                    style: TextStyle(fontSize: 14.0, color: Color(0xff333333)),
                  ),
                ),
              ],
            ),
          ),
        ),
      ),
    );
  }
}
