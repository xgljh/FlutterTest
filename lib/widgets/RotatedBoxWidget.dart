import 'package:flutter/material.dart';

class RotatedBoxWidget extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      home: Scaffold(
        appBar: AppBar(
          title: Text("RotatedBox"),
        ),
        body: Container(
          alignment: Alignment.center,
          color: Colors.blue,
          child: RotatedBox(
            quarterTurns: 1,
            child: Container(
              width: 50,
              height: 50,
              color: Colors.red,
              child: Text("Rotated"),
            ),
          ),
          width: 200,
          height: 200,
        ),
      ),
    );
  }
}
//const RotatedBox({
//Key key,
//@required this.quarterTurns,
//Widget child,
//})
