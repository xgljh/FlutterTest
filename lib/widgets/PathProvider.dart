import 'package:flutter/material.dart';
import 'package:path_provider/path_provider.dart';

class PathProviderDemo extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      home: Scaffold(
        appBar: AppBar(
          title: Text("path_provider"),
        ),
        body: ListView(
          children: <Widget>[
            RaisedButton(
              onPressed: () async {
                print(
                    "!!!!!!!!!!!!!!!${(await getExternalStorageDirectory()).path}");
              },
              child: Text("getExternalStorageDirectory"),
            ),
            RaisedButton(
              onPressed: () async {
                print(
                    "!!!!!!!!!!!!!!!${(await getTemporaryDirectory()).path}");
              },
              child: Text("getTemporaryDirectory"),
            ),
            RaisedButton(
              onPressed: () async {
                print(
                    "!!!!!!!!!!!!!!!${(await getApplicationDocumentsDirectory()).path}");
              },
              child: Text("getApplicationDocumentsDirectory"),
            ),
            RaisedButton(
              onPressed: () async {
                print(
                    "!!!!!!!!!!!!!!!${(await getApplicationSupportDirectory()).path}");
              },
              child: Text("getApplicationSupportDirectory"),
            ),
          ],
        ),
      ),
    );
  }
}
