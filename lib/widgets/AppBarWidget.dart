import 'package:flutter/material.dart';

class AppBarWidget extends StatefulWidget {
  @override
  State<StatefulWidget> createState() {
    return AppBarWidgetState();
  }
}

class AppBarWidgetState extends State<AppBarWidget>
    with SingleTickerProviderStateMixin {
  var tabs;
  TabController controller;

  @override
  void initState() {
    super.initState();
    tabs = [
      Tab(
        text: "tab1",
      ),
      Tab(
        text: "tab2",
      ),
      Tab(
        text: "tab3",
      ),
      Tab(
        text: "tab4",
      ),
      Tab(
        text: "tab5",
      ),
      Tab(
        text: "tab6",
      ),
      Tab(
        child: Container(
          child: Center(
            child: Text("tab7"),
          ),
          color: Colors.green,
        ),
      ),
    ];
    controller = TabController(length: tabs.length, vsync: this);
  }

  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      home: Scaffold(
        appBar: AppBar(
          leading: Icon(Icons.backspace),
          title: Text("标题"),
          centerTitle: true,
          actions: <Widget>[
            IconButton(
              icon: Icon(Icons.home),
              onPressed: () {},
            ),
            IconButton(
              icon: Icon(Icons.alarm),
              onPressed: () {},
            ),
          ],
          backgroundColor: Colors.grey,
          bottom: TabBar(
            labelColor: Colors.red,
            tabs: tabs,
            isScrollable: true,
            controller: controller,
          ),
        ),
      ),
    );
  }
}
//AppBar({
//Key key,
//this.leading,
//this.automaticallyImplyLeading = true,
//this.title,
//this.actions,
//this.flexibleSpace,
//this.bottom,
//this.elevation,
//this.shape,
//this.backgroundColor,
//this.brightness,
//this.iconTheme,
//this.actionsIconTheme,
//this.textTheme,
//this.primary = true,
//this.centerTitle,
//this.excludeHeaderSemantics = false,
//this.titleSpacing = NavigationToolbar.kMiddleSpacing,
//this.toolbarOpacity = 1.0,
//this.bottomOpacity = 1.0,
//})
