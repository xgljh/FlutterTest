import 'package:flutter/material.dart';

class ImageWidget extends StatefulWidget {
  @override
  State<StatefulWidget> createState() {
    return ImageWidgetState();
  }
}

class ImageWidgetState extends State<ImageWidget> {
  var neturl =
      "https://timgsa.baidu.com/timg?image&quality=80&size=b9999_10000&sec=1597162704796&di=ab997b5a7f7ca24d34b370842a3cc8e1&imgtype=0&src=http%3A%2F%2Fa2.att.hudong.com%2F36%2F48%2F19300001357258133412489354717.jpg";
  var gifurl =
      "https://timgsa.baidu.com/timg?image&quality=80&size=b9999_10000&sec=1597164080565&di=678035a6ddc56120bfab10f9f9e2833a&imgtype=0&src=http%3A%2F%2Fb-ssl.duitang.com%2Fuploads%2Fitem%2F201509%2F18%2F20150918203322_xeazR.gif";

  var tempUrl =
      "http://www.baidu.com/link?url=vmhi-4nuI__2r238kKz9WDoBvZEKYUhT5GCns7-lqobhF3joT1xHlKE8WcQytLoGnA7MSW-O2dyf0xSoRCeo2yHkDNL7Ketrznt5Mw9rOVCuLKdOwr9gywqISy0PiYS_Lfc29tVOQaWzdcOcqVXUQbed4iW0O8X-xIG4YoMXmV_py0aAVggBFJ0dxZZ3vuzhus9gU9x-BlgyZgFG9YtTDYN9CIuQ868YFeCpmqKuvxiZ3U-0Geyl0FcI83abVuT05VZWHktPVYbFvyx-WhR80n7TFbAAiQOLbyMRZd2Ljays1VaD_fMl913Ggj8c6Ybi6Ns-uAJsUw6-J86-7QTDO-U18-AzJbYXjq1OxsLiuVtxdTVmZ-J4baQgJOtRnBET665W9x1UdJbDOeqbQFRWETsBDhZoTHantxNz8VyVx1tQ105_AoGuwWOIHA388-FfDB3IzWU03DMTE5XjGMXGlbsUgCS2iol6mKIoLlHV9S58ICGospiP13Q0sFhCogYIbTwqKsmAb-x7Ya4DWpvjBL2lD-Oh6wVVB3Am7ZcuOpo7Zinjx_5oFzAy4IIPwuckbg6H3zpO7rA1STGnl3UqOAyrQ9Djr6XcCu91xCGa85S&timg=https%3A%2F%2Fss0.bdstatic.com%2F94oJfD_bAAcT8t7mm9GUKT-xh_%2Ftimg%3Fimage%26quality%3D100%26size%3Db4000_4000%26sec%3D1597152619%26di%3D6f2c4f2019eddb6cb7e4e2d1d94dc156%26src%3Dhttp%3A%2F%2Fa1.att.hudong.com%2F62%2F02%2F01300542526392139955025309984.jpg&click_t=1597152704547&s_info=1363_754&wd=&eqid=d4ce974e00007342000000035f329d6b";

  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      home: Scaffold(
          appBar: AppBar(
            title: Text("Image组件"),
          ),
          body: ListView(
            children: <Widget>[
              Container(
                color: Colors.grey,
                child: Column(
                  children: <Widget>[
                    Image(image: NetworkImage(neturl)),
//                    Image(image: NetworkImage(gifurl)),
                    Image.network(gifurl),
                    Container(
                      margin: EdgeInsets.only(top: 10),
                      color: Colors.red,
                      child: Image.network(
                        neturl,
                        width: 200,
                        height: 200,
                      ),
                    ),
                    Container(
                      margin: EdgeInsets.only(top: 10),
                      color: Colors.red,
                      child: Image.network(
                        neturl,
                        width: 200,
                        height: 200,
                        color: Colors.blue,
                        colorBlendMode: BlendMode.colorBurn,
                      ),
                    ),
                    Container(
                      margin: EdgeInsets.only(top: 10),
                      color: Colors.red,
                      child: Image.network(
                        neturl,
                        width: 100,
                        height: 100,
                        fit: BoxFit.fill,
                      ),
                    ),
                    Container(
                      margin: EdgeInsets.only(top: 10),
                      color: Colors.blue,
                      child: Image.network(
                        neturl,
                        width: 100,
                        height: 100,
                        fit: BoxFit.contain,
                      ),
                    ),
                    Container(
                      margin: EdgeInsets.only(top: 10),
                      color: Colors.green,
                      child: Image.network(
                        neturl,
                        width: 100,
                        height: 100,
                        fit: BoxFit.cover,
                      ),
                    ),
                    Container(
                      margin: EdgeInsets.only(top: 10),
                      color: Colors.blue,
                      child: Image.network(
                        neturl,
                        width: 100,
                        height: 100,
                        fit: BoxFit.scaleDown,
                      ),
                    ),
                    Container(
                      margin: EdgeInsets.only(top: 10),
                      color: Colors.blue,
                      child: Image.network(
                        neturl,
                        width: 100,
                        height: 100,
                        fit: BoxFit.fitWidth,
                      ),
                    ),

                    Container(
                      margin: EdgeInsets.only(top: 10),
                      color: Colors.blue,
                      child: Image.network(
                        neturl,
                        width: 100,
                        height: 100,
                        fit: BoxFit.fitHeight,
                      ),
                    ),
                    Container(
                      margin: EdgeInsets.only(top: 10),
                      color: Colors.red,
                      child: Image.asset(
                        "images/ic_launcher.png",
                        width: 100,
                        height: 100,
                      ),
                    ),
                    Container(
                      margin: EdgeInsets.only(top: 10),
                      color: Colors.orange,
                      child: Image.asset(
                        "images/icon_sqczcsj.png",
                        width: 100,
                        height: 100,
                        repeat: ImageRepeat.repeatX,
                      ),
                    ),
                    Container(
                      margin: EdgeInsets.only(top: 10),
                      color: Colors.orange,
                      child: Image.asset(
                        "images/icon_sqczcsj.png",
                        width: 100,
                        height: 100,
                        repeat: ImageRepeat.repeatY,
                      ),
                    ),
                    Container(
                      margin: EdgeInsets.only(top: 10),
                      color: Colors.orange,
                      child: Image.asset(
                        "images/icon_sqczcsj.png",
                        width: 100,
                        height: 100,
                        repeat: ImageRepeat.repeat,
                      ),
                    ),
                    Container(
                      margin: EdgeInsets.only(top: 10),
                      color: Colors.orange,
                      child: Image.asset(
                        "images/icon_sqczcsj.png",
                        width: 100,
                        height: 100,
                        alignment: Alignment.bottomCenter,
                      ),
                    ),
                    Container(
                      margin: EdgeInsets.only(top: 10),
                      color: Colors.green,
                      child: Image.asset(
                        "images/icon_sqczcsj.png",
                        width: 100,
                        height: 100,
                        alignment: Alignment.center,
                      ),
                    ),
                    Container(
                      margin: EdgeInsets.only(top: 10),
                      color: Colors.green,
                      child: Image.network(
                        neturl,
                        width: 100,
                        height: 100,
                        gaplessPlayback: true,
                      ),
                    ),
                    Container(
                      margin: EdgeInsets.only(top: 10),
                      color: Colors.green,
                      child: Image.network(
                        neturl,
                        width: 100,
                        height: 100,
                        gaplessPlayback: false,
                      ),
                    ),
                    Container(
                      margin: EdgeInsets.only(top: 10),
                      color: Colors.green,
                      child: Image.network(
                        tempUrl,
                        width: 100,
                        height: 100,
                        gaplessPlayback: false,
                      ),
                    ),
                    RaisedButton(
                      onPressed: () {
                        setState(() {
                          tempUrl =
                              "https://timgsa.baidu.com/timg?image&quality=80&size=b9999_10000&sec=1597162704796&di=ab997b5a7f7ca24d34b370842a3cc8e1&imgtype=0&src=http%3A%2F%2Fa2.att.hudong.com%2F36%2F48%2F19300001357258133412489354717.jpg";
                        });
                      },
                      child: Text("按钮"),
                    ),
                    FadeInImage.assetNetwork(
                        width: 100,
                        height: 100,
                        placeholder: "images/ic_launcher.png",
                        image: ""),
                    FadeInImage.assetNetwork(
                        width: 100,
                        height: 100,
                        placeholder: "images/ic_launcher.png",
                        image: neturl),
                    CircleAvatar(
                      foregroundColor: Color.fromARGB(255, 0, 255, 0),
                      child: Image.network(
                        neturl,
                        width: 100,
                        height: 100,
                      ),
//                      backgroundColor: Colors.red,
                      radius: 50,
                    ),
                    CircleAvatar(
                      radius: 50,
//                        child: ,
//                        backgroundColor: Colors.red,
                      backgroundImage: NetworkImage(neturl),
                    ),
                    CircleAvatar(
                      radius: 50,
                      child: Text("圆形"),
                      backgroundColor: Colors.red,
                    ),
                    Container(
                      width: 100,
                      height: 100,
                      decoration: BoxDecoration(
                          shape: BoxShape.circle,
                          image: DecorationImage(fit:BoxFit.fill,image: NetworkImage(neturl))),
                    ),
                    ClipRRect(
                      borderRadius: BorderRadius.circular(5.0),
                      child: Image.network(
                        neturl,
                        fit: BoxFit.fill,
                        width: 100,
                        height: 100,
                      ),
                    )
                  ],
                ),
              )
            ],
          )),
    );
  }
}

//BoxFit.fill 	    全图显示，显示可能拉伸，充满
//BoxFit.contain 	  全图显示，显示原比例，不需充满
//BoxFit.cover 	    显示可能拉伸，可能裁剪，充满
//BoxFit.fitWidth 	显示可能拉伸，可能裁剪，宽度充满
//BoxFit.fitHeight 	显示可能拉伸，可能裁剪，高度充满
//BoxFit.none
//BoxFit.scaleDown 	效果和contain差不多,但是此属性不允许显示超过源图片大小，可小不可大

//const Image({
//Key key,
//@required this.image,
//this.frameBuilder,
//this.loadingBuilder,
//this.errorBuilder,
//this.semanticLabel,
//this.excludeFromSemantics = false,
//this.width,
//this.height,
//this.color,
//this.colorBlendMode,
//this.fit,
//this.alignment = Alignment.center,
//this.repeat = ImageRepeat.noRepeat,
//this.centerSlice,
//this.matchTextDirection = false,
//this.gaplessPlayback = false,
//this.filterQuality = FilterQuality.low,
//})
