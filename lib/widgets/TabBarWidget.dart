import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';

class TabBarWidget extends StatefulWidget {
  @override
  State<StatefulWidget> createState() {
    return TabBarWidgetState();
  }
}

class TabBarWidgetState extends State<TabBarWidget>
    with SingleTickerProviderStateMixin {
  var tabs = <Tab>[];
  List<Tab> list = new List<Tab>();
  TabController controller;

  @override
  void initState() {
    super.initState();
    tabs.add(Tab(
      text: "tab1",
    ));
    tabs.add(Tab(
      text: "tab2",
    ));
    tabs.add(Tab(
      text: "tab3",
    ));
    tabs.add(Tab(
      text: "tab4",
    ));
    tabs.add(Tab(
      text: "tab5",
    ));
    tabs.add(Tab(
      text: "tab6",
    ));
    tabs.add(Tab(
      text: "tab7",
    ));
    controller = new TabController(length: tabs.length, vsync: this);
  }

  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      home: Scaffold(
          appBar: AppBar(
            backgroundColor: Colors.white,
            title: TabBar(
              tabs: tabs,
              controller: controller,
              isScrollable: true,
              labelPadding: EdgeInsets.only(right: 15, left: 50),
              labelColor: Colors.black,
              labelStyle: TextStyle(
                fontSize: 18,
              ),
              unselectedLabelColor: Colors.blue,
              unselectedLabelStyle: TextStyle(fontSize: 14),
              indicatorColor: Colors.red,
              indicatorWeight: 10,
              indicatorSize: TabBarIndicatorSize.label,
              indicatorPadding: EdgeInsets.only(right: 15, left: 50),
            ),
          ),
          body: TabBarView(
              controller: controller,
              children: tabs
                  .map((Tab tab) => Container(
                        child: Center(
                          child: Text(tab.text),
                        ),
                      ))
                  .toList())),
    );
  }

  @override
  void dispose() {
    super.dispose();
    controller.dispose();
  }
}

//const TabBar({
//Key key,
//@required this.tabs,
//this.controller,
//this.isScrollable = false,
//this.indicatorColor,
//this.indicatorWeight = 2.0,
//this.indicatorPadding = EdgeInsets.zero,
//this.indicator,
//this.indicatorSize,
//this.labelColor,
//this.labelStyle,
//this.labelPadding,
//this.unselectedLabelColor,
//this.unselectedLabelStyle,
//this.dragStartBehavior = DragStartBehavior.start,
//this.onTap,
//})
