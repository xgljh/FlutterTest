import 'package:flutter/material.dart';
import 'package:flutterapp/bean/ItemInfo.dart';
import 'package:flutterapp/widgets/ListITemWidget.dart';

class RefreshIndicatorWidget extends StatefulWidget {
  @override
  State<StatefulWidget> createState() {
    return RefreshIndicatorWidgetState();
  }
}

class RefreshIndicatorWidgetState extends State<RefreshIndicatorWidget> {
  var list = [];
  var isLoading = false;
  var showMore = false;
  var offState = false;
  var page = 0;

  var scroller = ScrollController();

  @override
  void initState() {
    super.initState();

    scroller.addListener(() {
      if (scroller.position.pixels == scroller.position.maxScrollExtent) {
        setState(() {
          showMore = true;
        });
        getMoreData();
      } else {
        setState(() {
          showMore = false;
        });
      }
    });

    getListData();
  }

  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      home: Scaffold(
        appBar: AppBar(
          title: Text("RefreshIndicator"),
        ),
        body: RefreshIndicator(
          onRefresh: _onRefresh,
          child: ListView.builder(
              controller: scroller,
              itemCount: list.length + 1,
              itemBuilder: (context, position) {
                return choiceItemWidget(context, position);
              }),
        ),
      ),
    );
  }

  Widget choiceItemWidget(BuildContext context, int position) {
    if (position < list.length) {
      return ListItemWidget(position, list[position], (position) {});
    } else {
      return showMoreLoadingWidget();
    }
  }

  void getMoreData() async {
    if (isLoading) {
      return;
    }
    setState(() {
      isLoading = true;
      page++;
    });
    print('上拉刷新开始,page = $page');
    await Future.delayed(Duration(seconds: 3), () {
      setState(() {
        isLoading = false;
        showMore = false;
        list.addAll(List.generate(3, (i) {
          return ItemInfo(text: "上拉添加ListView的一行数据$i");
        }));
        print('上拉刷新结束,page = $page');
      });
    });
  }

  void getListData() async {
    if (isLoading) return;
    setState(() {
      isLoading = true;
    });

    await Future.delayed(Duration(seconds: 3), () {
      setState(() {
        isLoading = false;
        list = List.generate(20, (index) => (ItemInfo(text: "第$index行数据")));
      });
    });
  }

  Widget showMoreLoadingWidget() {
    return Container(
      height: 50.0,
      child: Row(
        mainAxisAlignment: MainAxisAlignment.center,
        crossAxisAlignment: CrossAxisAlignment.center,
        children: <Widget>[
          Text(
            '加载中...',
            style: TextStyle(fontSize: 16.0),
          ),
        ],
      ),
    );
  }

  Future<void> _onRefresh() async {
    if (isLoading) return;
    setState(() {
      isLoading = true;
      page = 0;
    });

    await Future.delayed(Duration(seconds: 3), () {
      setState(() {
        isLoading = false;
        List temp = List.generate(3, (index) {
          return ItemInfo(text: "下拉添加ListView的一行数据$index");
        });
        temp.addAll(list);

        list = temp;
      });
    });
  }

  @override
  void dispose() {
    // TODO: implement dispose
    super.dispose();
    scroller.dispose();
  }
}

//
//const RefreshIndicator({
//Key key,
//@required this.child,
//this.displacement = 40.0,
//@required this.onRefresh,
//this.color,
//this.backgroundColor,
//this.notificationPredicate = defaultScrollNotificationPredicate,
//this.semanticsLabel,
//this.semanticsValue,
//this.strokeWidth = 2.0
//})
