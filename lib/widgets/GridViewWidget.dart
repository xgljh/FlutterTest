import 'package:flutter/material.dart';
import 'package:flutterapp/bean/ItemInfo.dart';

class GridViewWidget extends StatefulWidget {
  @override
  State<StatefulWidget> createState() {
    return GridViewWidgetState();
  }
}

typedef OnItemClickListener = void Function(int position);

class GridViewWidgetState extends State<GridViewWidget> {
  var list = <ItemInfo>[];

  @override
  void initState() {
    super.initState();
    list.add(ItemInfo(text: "文本1"));
    list.add(ItemInfo(text: "文本2"));
    list.add(ItemInfo(text: "文本3"));
    list.add(ItemInfo(text: "文本4"));
    list.add(ItemInfo(text: "文本5"));
    list.add(ItemInfo(text: "文本6"));
    list.add(ItemInfo(text: "文本7"));
    list.add(ItemInfo(text: "文本8"));
  }

  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      home: Scaffold(
          appBar: AppBar(
            title: Text("GridView"),
          ),
          body: GridView.custom(
            gridDelegate: SliverGridDelegateWithFixedCrossAxisCount(
                crossAxisCount: 2, mainAxisSpacing: 10, crossAxisSpacing: 10),
            childrenDelegate: SliverChildBuilderDelegate((context, position) {
              return Container(
                child: Text("1111111"),
                color: Colors.grey,
              );
            },childCount: 10),

          )),
    );
  }
}

class GridView2 extends StatelessWidget {
  var list;

  GridView2({this.list});

  @override
  Widget build(BuildContext context) {
    return GridView.builder(
        itemCount: list.length,
        gridDelegate: SliverGridDelegateWithFixedCrossAxisCount(
            crossAxisCount: 2, mainAxisSpacing: 10, crossAxisSpacing: 10),
        itemBuilder: (BuildContext context, int position) {
          return ItemView(
            list: list,
            postion: position,
            onItemClickListener: (position) {
              print("!!!!!!!!!!$position");
            },
          );
        });
  }
}

class ItemView extends StatelessWidget {
  OnItemClickListener onItemClickListener;
  var list;
  int postion;

  ItemView({this.onItemClickListener, this.list, this.postion});

  @override
  Widget build(BuildContext context) {
    return Container(
      color: Colors.grey,
      child: InkWell(
        child: Text(list[postion].text),
        onTap: () {
          onItemClickListener(postion);
        },
      ),
    );
  }
}

class GridView1 extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return GridView(
      gridDelegate: SliverGridDelegateWithFixedCrossAxisCount(
          crossAxisCount: 2,
          mainAxisSpacing: 10,
          crossAxisSpacing: 5,
          childAspectRatio: 2),
      children: <Widget>[
        Container(
          height: 20,
          color: Color(0xfff1f1f1),
          child: Text("文本1"),
        ),
        Container(
          color: Color(0xfff1f1f1),
          child: Text("文本1"),
        ),
        Container(
          color: Color(0xfff1f1f1),
          child: Text("文本1"),
        ),
        Container(
          color: Color(0xfff1f1f1),
          child: Text("文本1"),
        ),
        Container(
          color: Color(0xfff1f1f1),
          child: Text("文本1"),
        ),
      ],
    );
  }
}
//const SliverGridDelegateWithFixedCrossAxisCount({
//@required this.crossAxisCount,
//this.mainAxisSpacing = 0.0,
//this.crossAxisSpacing = 0.0,
//this.childAspectRatio = 1.0, // 宽高比，宽就是屏幕宽/ crossAxisCount - crossAxisSpacing
//})
