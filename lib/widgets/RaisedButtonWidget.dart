import 'package:flutter/material.dart';

class RaisedButtonWidget extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      home: Scaffold(
        appBar: AppBar(
          title: Text("RaisedButton"),
          centerTitle: true,
        ),
        body: Column(
          children: <Widget>[
            RaisedButton(
              onPressed: () {},
              textColor: Colors.red,
              color: Colors.grey,
              elevation: 5,
              padding: EdgeInsets.only(bottom: 10),
              child: Text("textColor 文字色，color背景色"),
            ),
            RaisedButton(
              onPressed: null,
              disabledTextColor: Colors.white,
              disabledColor: Colors.blue,
              disabledElevation: 10,
              child: Text(
                  "disabledTextColor 禁用时文字色，disabledColor 禁用时背景色,onPressed为null,不可用转态，不为null,就是可用状态"),
            ),
            RaisedButton(
              onPressed: () {
                return null;
              },
              autofocus: true,
              focusColor: Colors.red,
              hoverColor: Colors.blue,
              highlightColor: Colors.orange,
              child: Text("highlightColor 点击或者toch控件高亮的时候显示在控件上面，水波纹下面的颜色"),
            ),
            RaisedButton(
              onPressed: () {
                return null;
              },
              autofocus: true,
              focusColor: Colors.red,
              hoverColor: Colors.blue,
              highlightColor: Colors.orange,
              highlightElevation: 15,
              child: Text("highlightColor 点击或者toch控件高亮的时候显示在控件上面，水波纹下面的颜色"),
            ),
            RaisedButton(
              onPressed: () {
                return null;
              },
              shape: RoundedRectangleBorder(
                  borderRadius: BorderRadius.all(Radius.circular(10)),
                  side: BorderSide(color: Colors.red)),
              child: Text("shape RoundedRectangleBorder设置圆角边框"),
            ),
            RaisedButton(
              onPressed: () {
                return null;
              },
              shape: CircleBorder(
                  side:
                      BorderSide(color: Colors.blue, style: BorderStyle.solid)),
              child: Text("圆行边框"),
            ),
            Container(
              width: 60,
              height: 60,
              child: RaisedButton(
                onPressed: () {
                  return null;
                },
                shape: CircleBorder(
                    side: BorderSide(
                        color: Colors.blue, style: BorderStyle.solid)),
                child: Text("圆行边框"),
              ),
            ),
            Container(
              height: 100,
              width: 10,
              margin: EdgeInsets.only(top: 10),
              child: RaisedButton(
                onPressed: () {
                  return null;
                },
                child: Text("使用Container调节RaisedButton的宽高"),
              ),
            ),
          ],
        ),
      ),
    );
  }
}

//const RaisedButton({
//Key key,
//@required VoidCallback onPressed,
//VoidCallback onLongPress,
//ValueChanged<bool> onHighlightChanged,
//ButtonTextTheme textTheme,
//Color textColor,
//Color disabledTextColor,
//Color color,
//Color disabledColor,
//Color focusColor,
//Color hoverColor,
//Color highlightColor,
//Color splashColor,
//Brightness colorBrightness,
//double elevation,
//double focusElevation,
//double hoverElevation,
//double highlightElevation,
//double disabledElevation,
//EdgeInsetsGeometry padding,
//VisualDensity visualDensity,
//ShapeBorder shape,
//Clip clipBehavior = Clip.none,
//FocusNode focusNode,
//bool autofocus = false,
//MaterialTapTargetSize materialTapTargetSize,
//Duration animationDuration,
//Widget child,
//})
