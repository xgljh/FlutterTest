import 'package:flutter/material.dart';

class ExpandedWidget extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      home: Scaffold(
        appBar: AppBar(
          title: Text("Expanded和Flexible组件"),
        ),
        body: Column(
          children: <Widget>[
            Row(
              children: <Widget>[
                Container(
                  color: Colors.blue,
                  child: Text("Flexible"),
                ),
                Flexible(
                  child: Container(
                    color: Colors.red,
                    child: Text("Flexible"),
                  ),
                  flex: 1,
                ),
                Container(
                  color: Colors.blue,
                  child: Text("Flexible"),
                ),
              ],
            ),
            Row(
              children: <Widget>[
                Container(
                  color: Colors.blue,
                  child: Text("Flexible"),
                ),
                Flexible(
                  child: Container(
                    color: Colors.red,
                    child: Text("Flexible"),
                  ),
                  flex: 10,
                ),
                Flexible(
                  child: Container(
                    color: Colors.green,
                    child: Text("Flexible"),
                  ),
                  flex: 2,
                ),
              ],
            ),
            Row(
              children: <Widget>[
                Container(
                  color: Colors.blue,
                  child: Text("Expanded"),
                ),
                Expanded(
                  child: Container(
                    color: Colors.red,
                    child: Text("Expanded"),
                  ),
                  flex: 1,
                ),
                Expanded(
                  child: Container(
                    color: Colors.green,
                    child: Text("Expanded"),
                  ),
                  flex: 2,
                ),
              ],
            ),
          ],
        ),
      ),
    );
  }
}

//const Flexible({
//Key key,
//this.flex = 1,
//this.fit = FlexFit.loose,
//@required Widget child,
//})
