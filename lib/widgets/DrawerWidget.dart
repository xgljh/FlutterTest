import 'package:flutter/material.dart';

class DrawerWidget extends StatefulWidget {
  @override
  State<StatefulWidget> createState() {
    return DrawerWidgetState();
  }
}

class DrawerWidgetState extends State<DrawerWidget> {
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      home: Scaffold(
          appBar: AppBar(
            title: Text("Drawer"),
          ),
          body: Container(
            color: Color(0xff00ff00),
          ),
          drawer: DrawerWid()),
    );
  }
}

class DrawerWid extends StatelessWidget {
  var userAccountsDrawerHeader = UserAccountsDrawerHeader(
      accountName: Text("张三"),
      accountEmail: Text("123456@163.com"),
      currentAccountPicture: CircleAvatar(
        backgroundImage: NetworkImage(
            "http://img8.zol.com.cn/bbs/upload/23765/23764201.jpg"),
      ),
      otherAccountsPictures: <Widget>[
        CircleAvatar(
          backgroundImage: NetworkImage(
              "http://img8.zol.com.cn/bbs/upload/23765/23764201.jpg"),
        ),
        CircleAvatar(
          backgroundImage: NetworkImage(
              "http://img8.zol.com.cn/bbs/upload/23765/23764201.jpg"),
        ),
      ],
      onDetailsPressed: () {
        print("!!!!!!!!");
      });

  var drawerHeader = DrawerHeader(
    padding: EdgeInsets.zero,
    child: Stack(
      children: <Widget>[
        Image.network(
          "http://img8.zol.com.cn/bbs/upload/23765/23764201.jpg",
          height: 40,
          width: 200,
          fit: BoxFit.fitHeight,
        ),
        Align(
          alignment: FractionalOffset.bottomLeft,
          child: Container(
            padding: EdgeInsets.only(left: 12.0),
            height: 70.0,
            child: Row(
              mainAxisSize: MainAxisSize.min,
              children: <Widget>[
                CircleAvatar(
                  backgroundImage: NetworkImage(
                      "http://img8.zol.com.cn/bbs/upload/23765/23764201.jpg"),
                ),
                Container(
                    padding: EdgeInsets.only(left: 5.0),
                    child: Column(
                      mainAxisAlignment: MainAxisAlignment.center,
                      children: <Widget>[
                        Text("文本1"),
                        Text("文本2"),
                      ],
                    ))
              ],
            ),
          ),
        )
      ],
    ),
  );

  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      home: Scaffold(
        appBar: AppBar(
          title: Text("Drawer"),
        ),
        body: Container(
          color: Color(0xff00ff00),
        ),
        drawer: Drawer(
          child: ListView(
            children: <Widget>[
              drawerHeader,
              ListTile(
                title: Text("Title1"),
                subtitle: Text("Title1  subtitle"),
                leading: CircleAvatar(
                  child: Icon(Icons.home),
                ),
                onTap: () => print("Title1"),
              ),
              ListTile(
                title: Text("Title2"),
                leading: CircleAvatar(
                  child: Icon(Icons.alarm),
                ),
                selected: true,
                trailing: Icon(Icons.add),
                onTap: () => print("Title2"),
              ),
              ListTile(
                title: Text("Title3"),
                leading: CircleAvatar(
                  child: Icon(Icons.add),
                ),
                onTap: () => print("Title3"),
                enabled: false,
              ),
            ],
          ),
        ),
      ),
    );
  }
}
//const DrawerHeader({
//Key key,
//this.decoration,
//this.margin = const EdgeInsets.only(bottom: 8.0),
//this.padding = const EdgeInsets.fromLTRB(16.0, 16.0, 16.0, 8.0),
//this.duration = const Duration(milliseconds: 250),
//this.curve = Curves.fastOutSlowIn,
//@required this.child,
//})

//const DrawerHeader({
//Key key,
//this.decoration,
//this.margin = const EdgeInsets.only(bottom: 8.0),
//this.padding = const EdgeInsets.fromLTRB(16.0, 16.0, 16.0, 8.0),
//this.duration = const Duration(milliseconds: 250),
//this.curve = Curves.fastOutSlowIn,
//@required this.child,
//})

//const UserAccountsDrawerHeader({
//Key key,
//this.decoration,
//this.margin = const EdgeInsets.only(bottom: 8.0),
//this.currentAccountPicture,
//this.otherAccountsPictures,
//@required this.accountName,
//@required this.accountEmail,
//this.onDetailsPressed,
//this.arrowColor = Colors.white,
//})
