import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';

class SimpleDialogWidget extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return CupertinoPageScaffold(
        child: Container(
      margin: EdgeInsets.all(50),
      child: CupertinoButton(
          child: Text("按钮"),
          onPressed: () {
            showDialog(
              context: context,
              builder: (context) {
                return SimpleDialog(
                  title: Text("标题"),
                  children: <Widget>[
                    Text("一行"),
                    Text("一行"),
                    Text("一行"),
                    Text("又一行"),
                  ],
                  shape: RoundedRectangleBorder(
                      borderRadius: BorderRadius.all(Radius.circular(5))),
                );
              },
            );
          }),
    ));
  }
}

//const SimpleDialog({
//Key key,
//this.title,
//this.titlePadding = const EdgeInsets.fromLTRB(24.0, 24.0, 24.0, 0.0),
//this.children,
//this.contentPadding = const EdgeInsets.fromLTRB(0.0, 12.0, 0.0, 16.0),
//this.backgroundColor,
//this.elevation,
//this.semanticLabel,
//this.shape,
//})
