import 'package:flutter/material.dart';

class TextFieldWidget extends StatefulWidget {
  @override
  State<StatefulWidget> createState() {
    return TextFieldWidgetState();
  }
}

class TextFieldWidgetState extends State<TextFieldWidget> {
  TextEditingController controller = TextEditingController();

  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      home: Scaffold(
          appBar: AppBar(
            title: Text("TextFidld输入框"),
          ),
          body: Container(
            padding: EdgeInsets.only(left: 15, right: 15),
            child: ListView(
              children: <Widget>[
                Container(
                  color: Color.fromARGB(255, 241, 241, 241),
                  child: TextField(
                    keyboardType: TextInputType.number,
                    textInputAction: TextInputAction.search,
                    decoration: InputDecoration(
                        hintText:
                            "keyboardType 键盘类型,textInputAction 键盘右下角的操作按钮"),
                  ),
                ),
                Container(
                  color: Color.fromARGB(255, 241, 241, 241),
                  child: TextField(
                    keyboardType: TextInputType.number,
                    textInputAction: TextInputAction.search,
                    onChanged: (text) {
                      print("onChanged:${text}");
                    },
                    onSubmitted: (text) {
                      print("onSubmitted:${text}");
                    },
                    onEditingComplete: () {
                      print("onEditingComplete");
                    },
                    controller: controller,
                    decoration: InputDecoration(
                        hintText:
                            "onChanged输入监听,onEditingComplete当用户提交时调用，onSubmitted文字提交触发（键盘按键），controller编辑框的相关信息存储在里面"),
                  ),
                ),

                Container(
                  color: Color.fromARGB(255, 241, 241, 241),
                  child: TextField(
                    obscureText: true,
                    decoration: InputDecoration(hintText: "obscureText 密码"),
                  ),
                ),

                Container(
                  color: Color.fromARGB(255, 241, 241, 241),
                  child: TextField(
                    textAlign: TextAlign.right,
                    decoration: InputDecoration(hintText: "textAlign 对齐方式"),
                  ),
                ),
                Container(
                  color: Color.fromARGB(255, 241, 241, 241),
                  child: TextField(
                    textDirection: TextDirection.rtl,
                    decoration:
                        InputDecoration(hintText: "textDirection 输入内容显示方式"),
                  ),
                ),
                Container(
                  color: Color.fromARGB(255, 241, 241, 241),
                  child: TextField(
                    showCursor: true,
                    cursorWidth: 10,
                    cursorColor: Colors.cyanAccent,
                    cursorRadius: Radius.circular(1),
                    decoration: InputDecoration(hintText: "cursor 输入框"),
                  ),
                ),
                Container(
                  color: Color.fromARGB(255, 241, 241, 241),
                  child: TextField(
                    textAlign: TextAlign.right,
                    decoration: InputDecoration(icon: Icon(Icons.delete)),
                  ),
                ),
                Container(
                  margin: EdgeInsets.only(top: 10),
                  color: Color.fromARGB(255, 241, 241, 241),
                  child: TextField(
                    decoration: InputDecoration(
                        labelText: "labelText Material Design风格的输入提示",
                        labelStyle: TextStyle(color: Colors.red)),
                  ),
                ),
                Container(
                  margin: EdgeInsets.only(top: 10),
                  color: Color.fromARGB(255, 241, 241, 241),
                  child: TextField(
                    decoration: InputDecoration(
                        hintText: "hintText 普通输入提示",
                        hintStyle: TextStyle(color: Colors.blue)),
                  ),
                ),
                Container(
                  margin: EdgeInsets.only(top: 10),
                  color: Color.fromARGB(255, 241, 241, 241),
                  child: TextField(
                    decoration: InputDecoration(
                        errorText: "errorText 错误提示",
                        errorStyle: TextStyle(color: Colors.red)),
                  ),
                ),
                Container(
                  margin: EdgeInsets.only(top: 10),
                  color: Color.fromARGB(255, 241, 241, 241),
                  child: TextField(
                    decoration: InputDecoration(
                        helperText: "helperText 帮助提示",
                        helperStyle: TextStyle(color: Colors.green)),
                  ),
                ),
                Container(
                  margin: EdgeInsets.only(top: 10),
                  color: Color.fromARGB(255, 241, 241, 241),
                  child: TextField(
                    decoration: InputDecoration(
                        hintText: "请输入要搜索的内容",
                        contentPadding: EdgeInsets.only(left: 10, right: 10)),
                  ),
                ),
                Container(
                  margin: EdgeInsets.only(top: 10),
                  color: Color.fromARGB(255, 241, 241, 241),
                  child: TextField(
                    decoration: InputDecoration(
                      hintText: "请输入要搜索的内容",
                      contentPadding: EdgeInsets.only(left: 10, right: 10),
                      prefixIcon: Icon(Icons.delete),
                    ),
                  ),
                ),
                Container(
                  margin: EdgeInsets.only(top: 10),
                  color: Color.fromARGB(255, 241, 241, 241),
                  child: TextField(
                    decoration: InputDecoration(
                      labelText: "请输入要搜索的内容",
                      contentPadding: EdgeInsets.only(left: 10, right: 10),
                      prefixIcon: Icon(Icons.delete),
                    ),
                  ),
                ),
                Container(
                  margin: EdgeInsets.only(top: 10),
                  color: Color.fromARGB(255, 241, 241, 241),
                  child: TextField(
                    decoration: InputDecoration(
                      hintText: "请输入要搜索的内容,prefixText获取焦点时候显示",
                      hintStyle: TextStyle(),
                      contentPadding: EdgeInsets.only(left: 10, right: 10),
                      prefixText: "prefixText",
                    ),
                  ),
                ),
                Container(
                  margin: EdgeInsets.only(top: 10),
                  color: Color.fromARGB(255, 241, 241, 241),
                  child: TextField(
                    style: TextStyle(
                        textBaseline: TextBaseline.alphabetic, height: 1.0),
                    decoration: InputDecoration(
                      hintText: "请输入要搜索的内容",
                      hintStyle: TextStyle(
                          textBaseline: TextBaseline.alphabetic, height: 1.0),
                      contentPadding: EdgeInsets.only(left: 10, right: 10),
                      suffixIcon: Icon(Icons.delete),
                    ),
                  ),
                ),
                Container(
                  margin: EdgeInsets.only(top: 10),
                  color: Color.fromARGB(255, 241, 241, 241),
                  child: TextField(
                    style: TextStyle(textBaseline: TextBaseline.alphabetic),
                    textAlignVertical: TextAlignVertical.center,
                    decoration: InputDecoration(
                      hintText:
                          "suffixIcon,prefixIcon添加后输入框不居中，设置textAlignVertical: TextAlignVertical.center即可",
                      hintStyle:
                          TextStyle(textBaseline: TextBaseline.alphabetic),
                      contentPadding:
                          EdgeInsets.only(left: 10, right: 10, top: 1),
                      suffix: Icon(Icons.delete),
                    ),
                  ),
                ),
                Container(
                  margin: EdgeInsets.only(top: 10),
                  color: Color.fromARGB(255, 241, 241, 241),
                  child: TextField(
                    style: TextStyle(textBaseline: TextBaseline.alphabetic),
                    decoration: InputDecoration(
                      hintText:
                          "suffixIcon,prefixIcon添加后输入框不居中，在MaterialApp中添加：locale: const Locale('en')无效；textBaseline: TextBaseline.alphabetic 也无效；top调节或者自定义吧。。",
                      hintStyle:
                          TextStyle(textBaseline: TextBaseline.alphabetic),
                      contentPadding:
                          EdgeInsets.only(left: 10, right: 10, top: 10),
                      suffixIcon: Icon(Icons.delete),
                    ),
                  ),
                ),
                Container(
                  margin: EdgeInsets.only(top: 10),
                  color: Color.fromARGB(255, 241, 241, 241),
                  child: TextField(
                    decoration: InputDecoration(
                      hintText: "border OutlineInputBorder",
                      border: OutlineInputBorder(),
                    ),
                  ),
                ),
                Container(
                  margin: EdgeInsets.only(top: 10),
                  color: Color.fromARGB(255, 241, 241, 241),
                  child: TextField(
                    decoration: InputDecoration(
                      hintText:
                          "border InputBorder.none没有边框，UnderlineInputBorder下边框,OutlineInputBorder四周都有边框",
                      border: OutlineInputBorder(
                          // 设置border改变颜色还无效，什么鬼
                          borderSide: BorderSide(color: Colors.deepOrange)),
                      //改变边框颜色还得同时改变enabledBorder，focusedBorder，什么鬼
                      enabledBorder: OutlineInputBorder(
                          borderSide: BorderSide(color: Colors.deepOrange)),
                      focusedBorder: OutlineInputBorder(
                          borderSide: BorderSide(color: Colors.deepOrange)),
                    ),
                  ),
                ),
                Container(
                  margin: EdgeInsets.only(top: 10),
                  color: Color.fromARGB(255, 241, 241, 241),
                  child: TextField(
                    decoration: InputDecoration(
                      filled: false,
                      fillColor: Colors.red,
                      hintText: "filled 为true 显示fillColor",
                      border: InputBorder.none,
                    ),
                  ),
                ),
              ],
            ),
          )),
    );
  }
}

//const InputDecoration({
//this.icon,
//this.labelText,
//this.labelStyle,
//this.helperText,
//this.helperStyle,
//this.helperMaxLines,
//this.hintText,
//this.hintStyle,
//this.hintMaxLines,
//this.errorText,
//this.errorStyle,
//this.errorMaxLines,
//@Deprecated(
//'Use floatingLabelBehaviour instead. '
//'This feature was deprecated after v1.13.2.'
//)
//this.hasFloatingPlaceholder = true, // ignore: deprecated_member_use_from_same_package
//this.floatingLabelBehavior = FloatingLabelBehavior.auto,
//this.isDense,
//this.contentPadding,
//this.prefixIcon,
//this.prefixIconConstraints,
//this.prefix,
//this.prefixText,
//this.prefixStyle,
//this.suffixIcon,
//this.suffix,
//this.suffixText,
//this.suffixStyle,
//this.suffixIconConstraints,
//this.counter,
//this.counterText,
//this.counterStyle,
//this.filled,
//this.fillColor,
//this.focusColor,
//this.hoverColor,
//this.errorBorder,
//this.focusedBorder,
//this.focusedErrorBorder,
//this.disabledBorder,
//this.enabledBorder,
//this.border,
//this.enabled = true,
//this.semanticCounterText,
//this.alignLabelWithHint,
//})
