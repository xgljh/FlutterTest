import 'package:flutter/material.dart';

class DismissibleWidget extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      home: Scaffold(
        appBar: AppBar(
          title: Text("Dismissible组件"),
        ),
        body: ListView.builder(
          itemBuilder: (context, position) {
            return Dismissible(
              key: Key("positon$position"),
              child: ListTile(
                title: Text("标题$position"),
                leading: Icon(Icons.alarm),
                subtitle: Text("副标题"),
              ),
              background: Container(
                color: Colors.red,
                child: Text(
                  "删除",
                  style: TextStyle(color: Colors.white),
                ),
              ),
              secondaryBackground: Container(
                color: Colors.blue,
              ),
            );
          },
          itemCount: 5,
        ),
      ),
    );
  }
}

//const Dismissible({
//@required Key key,
//@required this.child,
//this.background,
//this.secondaryBackground,
//this.confirmDismiss,
//this.onResize,
//this.onDismissed,
//this.direction = DismissDirection.horizontal,
//this.resizeDuration = const Duration(milliseconds: 300),
//this.dismissThresholds = const <DismissDirection, double>{},
//this.movementDuration = const Duration(milliseconds: 200),
//this.crossAxisEndOffset = 0.0,
//this.dragStartBehavior = DragStartBehavior.start,
//})
