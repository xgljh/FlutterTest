import 'package:flutter/material.dart';

class FlatButtonWidget extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      home: Scaffold(
          appBar: AppBar(
            title: Text("FlatButton组件"),
          ),
          body: Column(
            children: <Widget>[
              FlatButton(onPressed: () {}, child: Text("FlatButton 透明背景")),
              RaisedButton(
                onPressed: () {},
                child: Text("RaisedButton"),
              ),
              MaterialButton(onPressed: (){},child: Text("MaterialButton"),),
              IconButton(icon: Icon(Icons.home), onPressed: (){},),

            ],
          )),
    );
  }
}

//const FlatButton({
//Key key,
//@required VoidCallback onPressed,
//VoidCallback onLongPress,
//ValueChanged<bool> onHighlightChanged,
//ButtonTextTheme textTheme,
//Color textColor,
//Color disabledTextColor,
//Color color,
//Color disabledColor,
//Color focusColor,
//Color hoverColor,
//Color highlightColor,
//Color splashColor,
//Brightness colorBrightness,
//EdgeInsetsGeometry padding,
//VisualDensity visualDensity,
//ShapeBorder shape,
//Clip clipBehavior = Clip.none,
//FocusNode focusNode,
//bool autofocus = false,
//MaterialTapTargetSize materialTapTargetSize,
//@required Widget child,
//})
