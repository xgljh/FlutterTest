import 'package:flutter/material.dart';

class SizeBoxWidget extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      home: Scaffold(
        appBar: AppBar(
          title: Text("SizeBox组件"),
        ),
        body: SizedBox(
          width: 100,
          height: 100,
          child: Container(
            width: 200,
            height: 300,
            child: Text("sizeBox"),
            color: Colors.red,
          ),
        ),
      ),
    );
  }
}
