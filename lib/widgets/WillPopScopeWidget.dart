import "package:flutter/material.dart";
import 'package:toast/toast.dart';

class WillPopScopeWidget extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    DateTime lastTime;
    return WillPopScope(
      onWillPop: () async {
        if (lastTime == null ||
            DateTime.now().difference(lastTime) > Duration(seconds: 1)) {
          lastTime = DateTime.now();
          Toast.show("双击退出", context);
          return false;
        }
        return true;
      },
      child: MaterialApp(
        home: Scaffold(
          appBar: AppBar(
            title: Text("WillPopScope"),
          ),
          body: Container(
            color: Colors.grey,
          ),
        ),
      ),
    );
  }
}

//const WillPopScope({
//Key key,
//@required this.child,
//@required this.onWillPop,
//})
