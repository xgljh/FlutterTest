import 'package:flutter/material.dart';

class SwitchWidget extends StatefulWidget {
  @override
  State<StatefulWidget> createState() {
    return SwitchWidgetState();
  }
}

class SwitchWidgetState extends State<SwitchWidget> {
  bool isCheck = false;
  bool isCheck1 = false;

  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      home: Scaffold(
        appBar: AppBar(
          title: Text("Switch"),
        ),
        body: Column(
          children: <Widget>[
            Switch(
              value: isCheck,
              onChanged: (isChecked) {
                setState(() {
                  isCheck = isChecked;
                });
              },
              activeColor: Colors.red,
              activeTrackColor: Colors.blue,
              inactiveThumbColor: Colors.green,
              inactiveTrackColor: Colors.orange,

//              activeThumbImage: AssetImage("imges/ic_launcher.png"),
//              inactiveThumbImage: AssetImage("images/icon_sqczcsj.png"),


//              focusColor: Colors.green,
//              hoverColor: Colors.orange,
            )
          ],
        ),
      ),
    );
  }
}
//const Switch({
//Key key,
//@required this.value,
//@required this.onChanged,
//this.activeColor,
//this.activeTrackColor,
//this.inactiveThumbColor,
//this.inactiveTrackColor,
//this.activeThumbImage,
//this.onActiveThumbImageError,
//this.inactiveThumbImage,
//this.onInactiveThumbImageError,
//this.materialTapTargetSize,
//this.dragStartBehavior = DragStartBehavior.start,
//this.focusColor,
//this.hoverColor,
//this.focusNode,
//this.autofocus = false,
//})
