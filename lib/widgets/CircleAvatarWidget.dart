import 'package:flutter/material.dart';

class CircleAvatarWidget extends StatefulWidget {
  @override
  State<StatefulWidget> createState() {
    return CircleAvatarWidgetState();
  }
}

class CircleAvatarWidgetState extends State<CircleAvatarWidget> {
  var neturl =
      "https://timgsa.baidu.com/timg?image&quality=80&size=b9999_10000&sec=1597162704796&di=ab997b5a7f7ca24d34b370842a3cc8e1&imgtype=0&src=http%3A%2F%2Fa2.att.hudong.com%2F36%2F48%2F19300001357258133412489354717.jpg";

  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      home: Scaffold(
          appBar: AppBar(
            title: Text("CircleAvatar组件"),
          ),
          body: Container(
            width: 300,
            color: Colors.orange,
            child: Column(
              mainAxisAlignment: MainAxisAlignment.center,
              crossAxisAlignment: CrossAxisAlignment.center,
              children: <Widget>[
                CircleAvatar(
                  child: Image.network(
                    neturl,
                    width: 100,
                    height: 100,
                  ),
                  radius: 50,
                ),
                CircleAvatar(
                  backgroundColor: Colors.red,
                  radius: 50,
                ),
                CircleAvatar(
                  radius: 50,
                  backgroundImage: NetworkImage(neturl),
                ),
                Container(
                  width: 100,
                  height: 100,
                  decoration: BoxDecoration(
                      shape: BoxShape.circle,
                      image: DecorationImage(
                          image: NetworkImage(neturl), fit: BoxFit.fill)),
                ),
                ClipOval(
                  child: Container(
                    width: 100,
                    height: 100,
                    child: Image.network(
                      neturl,
                      fit: BoxFit.fill,
                    ),
                  ),
                ),
                ClipRRect(
                  borderRadius: BorderRadius.circular(10),
                  child: Container(
                    width: 100,
                    height: 100,
                    child: Image.network(
                      neturl,
                      fit: BoxFit.fill,
                    ),
                  ),
                ),
                ClipRRect(
                  borderRadius: BorderRadius.circular(10),
                  child: Image.network(
                    neturl,
                    fit: BoxFit.fill,
                    width: 100,
                    height: 100,
                  ),
                )
              ],
            ),
          )),
    );
  }
}

//const CircleAvatar({
//Key key,
//this.child,
//this.backgroundColor,
//this.backgroundImage,
//this.onBackgroundImageError,
//this.foregroundColor,
//this.radius,
//this.minRadius,
//this.maxRadius,
//})
