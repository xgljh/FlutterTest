import 'package:flutter/material.dart';

class ShowModalBottomSheet extends StatefulWidget {
  @override
  State<StatefulWidget> createState() {
    return ShowModalBottomSheetState();
  }
}

class ShowModalBottomSheetState extends State<ShowModalBottomSheet> {
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      home: Scaffold(
        appBar: AppBar(title: Text("showModalBottomSheet函数")),
        body: RaisedButton(
          child: Text("按钮"),
          onPressed: () {
            var a = showModalBottomSheet(
                context: context,
//                isDismissible: false,
                backgroundColor: Colors.red,
                enableDrag: false,
                shape: Border(top: BorderSide(color: Colors.black,width: 1)),
//                shape: RoundedRectangleBorder(
//                    borderRadius: BorderRadius.circular(10)),
                builder: (context) {
                  return Container(
                      height: 200,
                      child: Column(
                        children: <Widget>[
                          RaisedButton(
                            onPressed: () {
                              Navigator.of(context).pop();
                            },
                            child: Text("关闭"),
                          ),
                        ],
                      ));
                });
          },
        ),
      ),
    );
  }
}

//Future<T> showModalBottomSheet<T>({
//@required BuildContext context,
//@required WidgetBuilder builder,
//Color backgroundColor,
//double elevation,
//ShapeBorder shape,
//Clip clipBehavior,
//Color barrierColor,
//bool isScrollControlled = false,
//bool useRootNavigator = false,
//bool isDismissible = true,
//bool enableDrag = true,
//})
