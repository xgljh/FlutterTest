import 'package:flutter/material.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutterapp/pages/OnePage.dart';
import 'package:flutterapp/pages/ThreePage.dart';
import 'package:flutterapp/pages/TwoPage.dart';

class CupertinoTabScaffoldWidget extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return CupertinoTabScaffold(
        tabBar: CupertinoTabBar(
          items: [
            BottomNavigationBarItem(
                icon: Icon(Icons.home),
                activeIcon: Icon(
                  Icons.home,
                  color: Colors.red,
                ),
                title: Text("首页")),
            BottomNavigationBarItem(
                icon: Icon(Icons.alarm),
                activeIcon: Icon(
                  Icons.alarm,
                  color: Colors.red,
                ),
                title: Text("产品")),
            BottomNavigationBarItem(
                icon: Icon(Icons.people),
                activeIcon: Icon(
                  Icons.people,
                  color: Colors.red,
                ),
                title: Text("我的"))
          ],
          activeColor: Colors.red,
          inactiveColor: Color(0xff333333),
          currentIndex: 1,
          onTap: (index) {
            print("!!!!!!!!!点击了$index");
          },
        ),
        tabBuilder: (context, index) {
          return CupertinoTabView(
            builder: (context) {
              switch (index) {
                case 0:
                  return OnePage();
                  break;
                case 1:
                  return TwoPage();
                  break;
                case 2:
                  return ThreePage();
                  break;
                default:
                  return OnePage();
              }
            },
          );
        });
  }
}

//const CupertinoTabView({
//Key key,
//this.builder,
//this.navigatorKey,
//this.defaultTitle,
//this.routes,
//this.onGenerateRoute,
//this.onUnknownRoute,
//this.navigatorObservers = const <NavigatorObserver>[],
//})

//const BottomNavigationBarItem({
//@required this.icon,
//this.title,
//Widget activeIcon,
//this.backgroundColor,
//})

//const CupertinoTabBar({
//Key key,
//@required this.items,
//this.onTap,
//this.currentIndex = 0,
//this.backgroundColor,
//this.activeColor,
//this.inactiveColor = _kDefaultTabBarInactiveColor,
//this.iconSize = 30.0,
//this.border = const Border(
//top: BorderSide(
//color: _kDefaultTabBarBorderColor,
//width: 0.0, // One physical pixel.
//style: BorderStyle.solid,
//),
//),
//})

//CupertinoTabScaffold({
//Key key,
//@required this.tabBar,
//@required this.tabBuilder,
//this.controller,
//this.backgroundColor,
//this.resizeToAvoidBottomInset = true,
//})
