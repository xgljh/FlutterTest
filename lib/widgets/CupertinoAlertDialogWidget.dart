import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';

class CupertinoAlertDialogWidget extends StatelessWidget {
  
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      home: Scaffold(
        appBar: AppBar(
          title: Text("CupertinoAlertDialog"),
        ),
        body: Center(
          child: CupertinoButton(
            child: Text("按钮"),
            onPressed: (){
              showDialog(context: context,builder: (context){
                return CupertinoAlertDialog(
                  title: Text("ios风格的弹窗"),
                  content: Container(
                    child: Column(
                      children: <Widget>[
                        Text("第一行"),
                        Text("第一行"),
                        Text("第一行"),
                        Text("第四行")
                      ],
                    ),
                  ),
                  actions: <Widget>[
                    CupertinoDialogAction(child: Text("确定",style: TextStyle(color: Colors.red),)),
                    CupertinoDialogAction(child: Text("取消"),onPressed: (){
                      Navigator.of(context).pop();
                    },),
                  ],
                );
              });
            },
            disabledColor: Colors.grey,
            color: Colors.blue,
          ),
        ),
      ),
    );
  }
}

//const CupertinoAlertDialog({
//Key key,
//this.title,
//this.content,
//this.actions = const <Widget>[],
//this.scrollController,
//this.actionScrollController,
//this.insetAnimationDuration = const Duration(milliseconds: 100),
//this.insetAnimationCurve = Curves.decelerate,
//})


//const CupertinoButton({
//Key key,
//@required this.child,
//this.padding,
//this.color,
//this.disabledColor = CupertinoColors.quaternarySystemFill,
//this.minSize = kMinInteractiveDimensionCupertino,
//this.pressedOpacity = 0.4,
//this.borderRadius = const BorderRadius.all(Radius.circular(8.0)),
//@required this.onPressed,
//})
