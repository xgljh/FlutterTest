import 'package:flutter/material.dart';

class PaddingWidget extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      home: Scaffold(
        appBar: AppBar(
          title: Text("Padding组件"),
        ),
        body: Container(
          width: 200,
          height: 150,
          color: Colors.green,
          child: Padding(
            padding: EdgeInsets.only(top: 10, left: 5,right: 15,bottom: 20),
            child: Container(
              width: 50,
              height: 50,
              color: Colors.red,
              child: Text("padding"),
            ),
          ),
        ),
      ),
    );
  }
}

//const Padding({
//Key key,
//@required this.padding,
//Widget child,
//})
