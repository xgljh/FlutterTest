import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';

class TableWidget extends StatefulWidget {
  @override
  State<StatefulWidget> createState() {
    return TableWidgetState();
  }
}

class TableWidgetState extends State<TableWidget> {
  var tabs = <TableRow>[
    TableRow(children: <Widget>[
      Text("第一行，第一列"),
      Text("第一行，第2列"),
      Text("第一行，第3列"),
      Text("第一行，第4列")
    ]),
    TableRow(
        decoration: BoxDecoration(
            color: Colors.blue,
            border: Border(
                top: BorderSide(color: Colors.red, width: 3),
                right: BorderSide(color: Colors.black, width: 2))),
        children: <Widget>[
          Text("第2行，第一列"),
          Container(
            child: Text("第2行，第2列"),
            decoration: BoxDecoration(
                border: Border(right: BorderSide(color: Colors.red, width: 1))),
          ),
          Text("第2行，第3列"),
          Text("第2行，第4列")
        ]),
    TableRow(
        decoration: BoxDecoration(
//            color: Colors.orange,
            border: Border(
          top: BorderSide(color: Colors.amberAccent, width: 3),
        )),
        children: <Widget>[
          Text("第3行，第一列"),
          Text("第3行，第2列"),
          Text("第3行，第3列"),
          Text("第3行，第4列")
        ]),
  ];

  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      home: Scaffold(
          appBar: AppBar(
            title: Text("Tabble组件"),
          ),
          body: Container(
            padding: EdgeInsets.all(20),
            child: Table(
              children: tabs,
              columnWidths: <int, TableColumnWidth>{
                0: FixedColumnWidth(50),
                1: FixedColumnWidth(100),
                2: FixedColumnWidth(80),
              },
              border: TableBorder(
                  top: BorderSide(color: Colors.green, width: 5),
                  bottom: BorderSide(color: Colors.grey, width: 5),
                  right: BorderSide(
                      color: Colors.red, width: 1, style: BorderStyle.solid),
                  left: BorderSide(
                      color: Colors.blue, width: 2, style: BorderStyle.solid)),
            ),
          )),
    );
  }
}

//Table({
//Key key,
//this.children = const <TableRow>[],
//this.columnWidths,
//this.defaultColumnWidth = const FlexColumnWidth(1.0),
//this.textDirection,
//this.border,
//this.defaultVerticalAlignment = TableCellVerticalAlignment.top,
//this.textBaseline,
//})
