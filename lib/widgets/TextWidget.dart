import 'package:flutter/gestures.dart';
import 'package:flutter/material.dart';

class TextWidget extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      home: Scaffold(
        appBar: AppBar(
          title: Text("Text组件"),
          centerTitle: true,
          leading: InkWell(
            child: Icon(
              Icons.backspace,
              color: Colors.white,
            ),
            onTap: () {
              Navigator.pop(context);
            },
          ),
        ),
        body: ListView(
          children: <Widget>[
            Container(
                height: 40,
                color: Color.fromARGB(255, 241, 241, 241),
                child: Text(
                  "文本对齐方式 TextAlign.center,只会水平居中",
                  textAlign: TextAlign.center,
                )),
            Container(
                height: 40,
                margin: EdgeInsets.only(top: 10),
                color: Color.fromARGB(255, 241, 241, 241),
                child: Center(
                  child: Text(
                    "文本对齐方式 TextAlign.center,包层center，right失效",
                    textAlign: TextAlign.right,
                  ),
                )),
            Container(
              height: 40,
              margin: EdgeInsets.only(top: 10),
              color: Color.fromARGB(255, 241, 241, 241),
              child: Text(
                "文本对齐方式 TextAlign.center",
                textAlign: TextAlign.right,
              ),
            ),
            Container(
                height: 40,
                margin: EdgeInsets.only(top: 10),
                color: Color.fromARGB(255, 241, 241, 241),
                child: Align(
                  alignment: Alignment.centerRight,
                  child: Text(
                    "文本对齐方式 TextAlign.center",
                    textAlign: TextAlign.right,
                  ),
                )),
            Container(
              height: 40,
              margin: EdgeInsets.only(top: 10),
              color: Color.fromARGB(255, 241, 241, 241),
              child: Text(
                "softWrap是否自动换行 true，呵呵呵呵呵呵呵呵呵呵呵呵呵呵呵呵呵呵呵呵呵呵呵呵呵呵呵呵呵",
                softWrap: true,
              ),
            ),
            Container(
              height: 40,
              margin: EdgeInsets.only(top: 10),
              color: Color.fromARGB(255, 241, 241, 241),
              child: Text(
                "softWrap是否自动换行 false，呵呵呵呵呵呵呵呵呵呵呵呵呵呵呵呵呵呵呵呵呵呵呵呵呵呵呵呵",
                softWrap: false,
              ),
            ),
            Container(
              height: 40,
              margin: EdgeInsets.only(top: 10),
              color: Color.fromARGB(255, 241, 241, 241),
              child: Text(
                "softWrap: false，TextOverflow.clip 文字超出屏幕之后的处理方式文字超出屏幕之后的处理方式",
                softWrap: false,
                overflow: TextOverflow.clip,
              ),
            ),
            Container(
              height: 40,
              margin: EdgeInsets.only(top: 10),
              color: Color.fromARGB(255, 241, 241, 241),
              child: Text(
                "softWrap: false，TextOverflow.ellipsis 文字超出屏幕之后的处理方式文字超出屏幕之后的处理方式",
                softWrap: false,
                overflow: TextOverflow.ellipsis,
              ),
            ),
            Container(
              height: 40,
              margin: EdgeInsets.only(top: 10),
              color: Color.fromARGB(255, 241, 241, 241),
              child: Text(
                "softWrap: false，TextOverflow.fade 文字超出屏幕之后的处理方式文字超出屏幕之后的处理方式",
                softWrap: false,
                overflow: TextOverflow.fade,
              ),
            ),
            Container(
              height: 40,
              margin: EdgeInsets.only(top: 10),
              color: Color.fromARGB(255, 241, 241, 241),
              child: Text(
                "softWrap: false，TextOverflow.visible 文字超出屏幕之后的处理方式文字超出屏幕之后的处理方式",
                softWrap: false,
                overflow: TextOverflow.visible,
              ),
            ),
            Container(
              height: 40,
              margin: EdgeInsets.only(top: 10),
              color: Color.fromARGB(255, 241, 241, 241),
              child: Text(
                "textScaleFactor字体缩放倍数1",
                textScaleFactor: 1,
              ),
            ),
            Container(
              height: 40,
              margin: EdgeInsets.only(top: 10),
              color: Color.fromARGB(255, 241, 241, 241),
              child: Text(
                "textScaleFactor字体缩放倍数2",
                textScaleFactor: 2,
              ),
            ),
            Container(
              height: 40,
              margin: EdgeInsets.only(top: 10),
              color: Color.fromARGB(255, 241, 241, 241),
              child: Text(
                "maxLines 文字最多显示行数,可以和overflow配合使用，TextOverflow.ellipsis，在最后一行末尾显示省略号",
                maxLines: 1,
                overflow: TextOverflow.ellipsis,
              ),
            ),
            Container(
              height: 40,
              margin: EdgeInsets.only(top: 10),
              color: Color.fromARGB(255, 241, 241, 241),
              child: Text(
                "文字方向TextDirection.rtl",
                textDirection: TextDirection.rtl,
              ),
            ),
            Container(
              height: 40,
              margin: EdgeInsets.only(top: 10),
              color: Color.fromARGB(255, 241, 241, 241),
              child: Text(
                "文字方向TextDirection.ltr",
                textDirection: TextDirection.ltr,
              ),
            ),
            Container(
                height: 40,
                margin: EdgeInsets.only(top: 10),
                color: Color.fromARGB(255, 241, 241, 241),
                child: Text(
                  "文字大小fontSize,文字颜色color,背景色backgroundColor",
                  style: TextStyle(
                      fontSize: 13,
                      color: Colors.red,
                      backgroundColor: Colors.blue),
                )),
            Container(
              height: 40,
              margin: EdgeInsets.only(top: 10),
              color: Color.fromARGB(255, 241, 241, 241),
              child: Text(
                  "字体粗细 fontWeight,字体粗细  w100 ~ w900 和粗体与正常，正常是w400,粗体是w700",
                  style: TextStyle(fontSize: 12, fontWeight: FontWeight.bold)),
            ),
            Container(
              height: 40,
              margin: EdgeInsets.only(top: 10),
              color: Color.fromARGB(255, 241, 241, 241),
              child: Text(
                  "文字样式fontStyle, FontStyle.italic 斜体和FontStyle.normal正常",
                  style: TextStyle(fontStyle: FontStyle.italic)),
            ),
            Container(
              height: 40,
              margin: EdgeInsets.only(top: 10),
              color: Color.fromARGB(255, 241, 241, 241),
              child: Text("文字间距letterSpacing，hello word",
                  style: TextStyle(letterSpacing: 5, wordSpacing: 10)),
            ),
            Container(
              height: 40,
              margin: EdgeInsets.only(top: 10),
              color: Color.fromARGB(255, 241, 241, 241),
              child: Text(
                "文字基线 textBaseline TextBaseline，hello word",
                style: TextStyle(textBaseline: TextBaseline.alphabetic),
              ),
            ),
            Container(
              height: 40,
              margin: EdgeInsets.only(top: 10),
              color: Color.fromARGB(255, 241, 241, 241),
              child: Text(
                "文字基线 textBaseline TextBaseline，hello word",
                style: TextStyle(textBaseline: TextBaseline.ideographic),
              ),
            ),
            Container(
              height: 40,
              margin: EdgeInsets.only(top: 10),
              color: Color.fromARGB(255, 241, 241, 241),
              child: Text(
                "height会乘以fontSize做为行高",
                style: TextStyle(height: 2),
              ),
            ),
            Container(
              height: 40,
              margin: EdgeInsets.only(top: 10),
              color: Color.fromARGB(255, 241, 241, 241),
              child: Text(
                "decoration三种下划线underline、lineThrough删除线、overline上划线,decorationStyle 线的形状",
                style: TextStyle(
                    decoration: TextDecoration.lineThrough,
                    decorationColor: Colors.blue,
                    decorationStyle: TextDecorationStyle.wavy),
              ),
            ),
            Container(
              height: 40,
              margin: EdgeInsets.only(top: 10),
              color: Color.fromARGB(255, 241, 241, 241),
              child: Text.rich(TextSpan(text: "可以放多种样式", children: <TextSpan>[
                TextSpan(
                    text: "改变颜色",
                    style: TextStyle(color: Colors.red),
                    recognizer: TapGestureRecognizer()
                      ..onTap = () {
                        print("点击改变颜色");
                      }),
                TextSpan(
                    text: "改变大小",
                    style: TextStyle(fontSize: 11),
                    recognizer: TapGestureRecognizer()
                      ..onTap = () {
                        print("改变大小");
                      }),
                TextSpan(
                    text: "添加下划线",
                    style: TextStyle(
                      decoration: TextDecoration.underline,
                    )),
              ])),
            ),
          ],
        ),
      ),
    );
  }
}
//const Text(
//this.data,//this.data 要显示的文本内容
//    {Key key,
//this.style,
//this.textAlign,//文本对齐方式
//this.textDirection,//文本方向
//this.locale,
//this.softWrap,//是否自动换行 false文字不考虑容器大小，单行显示，超出屏幕部分将默认截断处理
//this.overflow,//文字超出屏幕之后的处理方式，TextOverflow.clip剪裁   TextOverflow.fade 渐隐  TextOverflow.ellipsis省略号
//this.textScaleFactor,//文本字体的缩放倍数，如：1.5则在默认字体上变成1.5倍大小字体，0.5则是0.5倍
//this.maxLines,//文本最多显示多少行
//this.semanticsLabel,
//}) : assert(data != null),
//textSpan = null,
//super(key: key);
//
//
//const TextStyle({
//this.inherit = true, //默认为true，设置为false时候表示不显示
//this.color,//文字颜色
//this.fontSize,//文字大小 ，默认是14.0的
//this.fontWeight,//字体粗细  粗体和正常
//this.fontStyle,//文字样式，normal正常 italic 斜体
//this.letterSpacing,//字母间隙(负值可以让字母更紧凑)
//this.wordSpacing,//单词间隙(负值可以让单词更紧凑)
//this.textBaseline, //alphabetic：用于对齐字母字符底部的水平线,  ideographic：用于对齐表意字符的水平线
//this.height,  //用在Text控件上的时候，会乘以fontSize做为行高
//this.locale,//国际化
//this.foreground,//用paint来渲染text，也可以用他来改变字体颜色等
//this.background,
//this.decoration, //四种下划线、lineThrough删除线、overline上划线，默认是无
//this.decorationColor,//线的颜色
//this.decorationStyle,//文字装饰的风格  dashed虚线大间隔,dotted虚线小间隔，double在underline和lineThrough时两条线，在overline时三条线，solid在underline和solidlineThrough时一条线,在overline两条线
//this.debugLabel,
//String fontFamily,
//String package,
//}) : fontFamily = package == null ? fontFamily : 'packages/$package/$fontFamily',
//assert(inherit != null),
//assert(identical(color, null) || identical(foreground, null), _kColorForegroundWarning);
