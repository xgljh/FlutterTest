import 'package:flutter/material.dart';
import 'package:flutter/cupertino.dart';

class CupertinoPageScaffoldWidget extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return CupertinoPageScaffold(
        navigationBar: CupertinoNavigationBar(
//          leading: Icon(Icons.backspace),
          middle: Text("标题"),
          trailing: Text("分享"),
          backgroundColor: Color(0xfff1f1f1),
          previousPageTitle: "关闭",
        ),
        child: Container(
          color: Colors.red,
        ));
  }
}

//const CupertinoNavigationBar({
//Key key,
//this.leading,
//this.automaticallyImplyLeading = true,
//this.automaticallyImplyMiddle = true,
//this.previousPageTitle,
//this.middle,
//this.trailing,
//this.border = _kDefaultNavBarBorder,
//this.backgroundColor,
//this.brightness,
//this.padding,
//this.actionsForegroundColor,
//this.transitionBetweenRoutes = true,
//this.heroTag = _defaultHeroTag,
//})

//const CupertinoPageScaffold({
//Key key,
//this.navigationBar,
//this.backgroundColor,
//this.resizeToAvoidBottomInset = true,
//@required this.child,
//})
