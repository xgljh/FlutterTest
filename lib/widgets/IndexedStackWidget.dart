import 'package:flutter/material.dart';

class IndexedStackWidget extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
        home: Scaffold(
      appBar: AppBar(
        title: Text("IndexedStack"),
      ),
      body: Container(
          width: 200,
          height: 200,
          color: Colors.orange,
          child: IndexedStack(
            index: 2,
            children: <Widget>[
              Container(
                width: 100,
                height: 100,
                color: Colors.red,
              ),
              Container(
                width: 80,
                height: 80,
                color: Colors.blue,
              ),
              Container(
                width: 60,
                height: 60,
                color: Colors.green,
              ),
            ],
          )),
    ));
  }
}

//IndexedStack({
//Key key,
//AlignmentGeometry alignment = AlignmentDirectional.topStart,
//TextDirection textDirection,
//StackFit sizing = StackFit.loose,
//this.index = 0,
//List<Widget> children = const <Widget>[],
//})
