import 'package:flutter/material.dart';

class OpacityWidget extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      home: Scaffold(
        appBar: AppBar(
          title: Text("Opacity组件"),
        ),
        body: Container(
          width: 100,
          height: 100,
          color: Colors.red,
          child: Center(
            child: Opacity(
              opacity: 0.5,
              child: Container(
                width: 50,
                height: 50,
                color: Colors.green,
              ),
            ),
          ),
        ),
      ),
    );
  }
}
