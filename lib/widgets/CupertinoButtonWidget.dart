import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';

class CupertinoButtonWidget extends StatelessWidget {

  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      home: Scaffold(
        appBar: AppBar(
          title: Text("CupertinoButton"),
        ),
        body: Center(
          child: CupertinoButton(
            child: Text("按钮"),
            onPressed: null,
            disabledColor: Colors.grey,
            color: Colors.blue,
          ),
        ),
      ),
    );
  }
}

//const CupertinoButton({
//Key key,
//@required this.child,
//this.padding,
//this.color,
//this.disabledColor = CupertinoColors.quaternarySystemFill,
//this.minSize = kMinInteractiveDimensionCupertino,
//this.pressedOpacity = 0.4,
//this.borderRadius = const BorderRadius.all(Radius.circular(8.0)),
//@required this.onPressed,
//})
