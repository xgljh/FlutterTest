import 'package:flutter/material.dart';

class StackWidget extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
        home: Scaffold(
      appBar: AppBar(
        title: Text("Stack"),
      ),
      body: Column(
        children: <Widget>[
          Container(
            width: 100,
            height: 100,
            color: Colors.grey,
            child: Stack(
              fit: StackFit.loose,
              children: <Widget>[
                Container(
                  width: 50,
                  height: 50,
                  color: Colors.red,
                )
              ],
            ),
          ),
          Container(
            width: 100,
            height: 100,
            margin: EdgeInsets.only(top: 10),
            color: Colors.grey,
            child: Stack(
              fit: StackFit.expand,
              children: <Widget>[
                Container(
                  width: 50,
                  height: 50,
                  color: Colors.red,
                )
              ],
            ),
          ),
          Container(
            width: 100,
            height: 100,
            margin: EdgeInsets.only(top: 10),
            color: Colors.grey,
            child: Stack(
              overflow: Overflow.clip,
              children: <Widget>[
                Container(
                  width: 200,
                  height: 200,
                  color: Colors.red,
                )
              ],
            ),
          ),
          Container(
            width: 100,
            height: 100,
            margin: EdgeInsets.only(top: 10),
            color: Colors.grey,
            child: Stack(
              fit: StackFit.loose,
              overflow: Overflow.visible,
              children: <Widget>[
                Container(
                  width: 200,
                  height: 200,
                  color: Colors.red,
                )
              ],
            ),
          ),
          Container(
            width: 100,
            height: 100,
            margin: EdgeInsets.only(top: 10),
            color: Colors.grey,
            child: Stack(
              fit: StackFit.expand,
              children: <Widget>[
                Container(
                  width: 200,
                  height: 200,
                  color: Colors.red,
                )
              ],
            ),
          ),
        ],
      ),
    ));
  }
}
//Stack({
//Key key,
//this.alignment = AlignmentDirectional.topStart,
//this.textDirection,
//this.fit = StackFit.loose,   //StackFit.loose 子Widget多大就多大，StackFit.expand 子Widget的大小和父组件一样大
//this.overflow = Overflow.clip, //子Widget 超出Stack时候如何显示，默认值是Overflow.clip，子Widget超出Stack会被截断，Overflow.visible超出部分还会显示的
//List<Widget> children = const <Widget>[],
//})
