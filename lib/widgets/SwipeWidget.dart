import 'package:flutter/material.dart';
import 'package:flutter_swiper/flutter_swiper.dart';

class SwipeWidget extends StatefulWidget {
  @override
  State<StatefulWidget> createState() {
    return SwipeWidgetState();
  }
}

class SwipeWidgetState extends State<SwipeWidget> {
  var list = [
    "http://img8.zol.com.cn/bbs/upload/23765/23764201.jpg",
    "http://img8.zol.com.cn/bbs/upload/23765/23764201.jpg"
  ];

  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      home: Scaffold(
        appBar: AppBar(
          title: Text("开源swipe组件"),
        ),
        body: Container(
          height: 100,
          color: Colors.grey,
          child: Swiper(
            itemCount: list.length,
            itemBuilder: (BuildContext context, int position) {
              return Image.network(
                list[position],
                fit: BoxFit.fill,
              );
            },
//            itemHeight: 200,
//            autoplayDelay: 2000,
            viewportFraction:0.8,
//            scale: 0.9,
            autoplay: true,
            duration: 500,
            pagination: new SwiperPagination(
                alignment: Alignment.bottomRight,
                builder: DotSwiperPaginationBuilder(
                    size: 20, activeColor: Colors.orange, color: Colors.red)),
            control: new SwiperControl(iconNext: Icons.next_week),
            onIndexChanged: (index) {
              print("!!!!!!$index");
            },
            onTap: (index) {
              print("!!!!!!$index");
            },
          ),
        ),
      ),
    );
  }
}

//Swiper({
//this.itemBuilder,
//this.indicatorLayout: PageIndicatorLayout.NONE,
//
/////
//this.transformer,
//@required this.itemCount,
//this.autoplay: false,
//this.layout: SwiperLayout.DEFAULT,
//this.autoplayDelay: kDefaultAutoplayDelayMs,
//this.autoplayDisableOnInteraction: true,
//this.duration: kDefaultAutoplayTransactionDuration,
//this.onIndexChanged,
//this.index,
//this.onTap,
//this.control,
//this.loop: true,
//this.curve: Curves.ease,
//this.scrollDirection: Axis.horizontal,
//this.pagination,
//this.plugins,
//this.physics,
//Key key,
//this.controller,
//this.customLayoutOption,
//
///// since v1.0.0
//this.containerHeight,
//this.containerWidth,
//this.viewportFraction: 1.0,//item占的比例，左右会出现上一个和下一个item
//this.itemHeight,
//this.itemWidth,
//this.outer: false,
//this.scale,
//this.fade,
//})
