import 'package:flutter/material.dart';

class AspectRatioWidget extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      home: Scaffold(
        appBar: AppBar(
          title: Text("AspectRation"),
        ),
        body: Container(
          color: Colors.red,
          width: 100,
          child: AspectRatio(
            aspectRatio: 1/3,//16:9 width:height
            child: Container(//子控件的宽高无效
              color: Colors.black,
              width: 100,
              height: 100,
            ),
          ),
        ),
      ),
    );
  }
}
