import 'package:flutter/material.dart';

class BoxShadowWidget extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      home: Scaffold(
        appBar: AppBar(
          title: Text("BoxShadow"),
        ),
        body: Column(
          children: <Widget>[
            Container(
              decoration: BoxDecoration(color: Colors.black, boxShadow: [
                BoxShadow(
                    color: Color(0xffff0000),
                    offset: Offset(4.0, 5.0),
                    blurRadius: 6.0),
                BoxShadow(
                    color: Color(0xff00ff00),
                    offset: Offset(3.0, 4.0),
                    blurRadius: 3.0),
                BoxShadow(
                    color: Color(0xff0000ff),
                    offset: Offset(2.0, 3.0),
                    spreadRadius: 2.0),
              ]),
              child: Center(
                child: Text("呵呵"),
              ),
              width: 100,
              height: 100,
              margin: EdgeInsets.only(bottom: 20, left: 20),
            )
          ],
        ),
      ),
    );
  }
}

//const BoxShadow({
//Color color = const Color(0xFF000000),//阴影默认颜色,不能与父容器同时设置color
//Offset offset = Offset.zero,//延伸的阴影，向右下偏移的距离
//double blurRadius = 0.0,//延伸距离,会有模糊效果
//this.spreadRadius = 0.0,延伸距离,不会有模糊效果
//})
