import 'package:flutter/material.dart';

class DecorationBoxWidget extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      home: Scaffold(
        appBar: AppBar(
          title: Text("DecotationBox"),
        ),
        body: DecoratedBox(
          position: DecorationPosition.background,
          child: Padding(
            padding: EdgeInsets.only(left: 10, right: 10, top: 3, bottom: 3),
            child: Text("DecoratedBox"),
          ),
          decoration: BoxDecoration(
              border: Border.all(
                  color: Colors.red, width: 1, style: BorderStyle.solid),
              borderRadius: BorderRadius.circular(10),
              color: Color(0x0000ff00)),
        ),
      ),
    );
  }
}

//const DecoratedBox({
//Key key,
//@required this.decoration,
//this.position = DecorationPosition.background,
//Widget child,
//})

//const BoxDecoration({
//this.color,
//this.image,
//this.border,
//this.borderRadius,
//this.boxShadow,
//this.gradient,
//this.backgroundBlendMode,
//this.shape = BoxShape.rectangle,
//})
