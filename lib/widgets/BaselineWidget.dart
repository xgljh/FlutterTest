import 'package:flutter/material.dart';

class BaselineWidget extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      home: Scaffold(
        appBar: AppBar(
          title: Text("Baseline组件"),
        ),
        body: Container(
          color: Colors.blue,
          child: Row(
            children: <Widget>[
              Container(
                color: Colors.red,
                child: Baseline(
                  baseline: 40,
                  baselineType: TextBaseline.alphabetic,
                  child: Text("文本"),
                ),
              ),
              Container(
                child: Baseline(
                  baseline: 0,
                  baselineType: TextBaseline.alphabetic,
                  child: Text("adgdvdf"),
                ),
              ),
              Container(
                child: Baseline(
                  baseline: 20,
                  baselineType: TextBaseline.ideographic,
                  child: Text("文本", style: TextStyle(fontSize: 20)),
                ),
              ),
              Container(
                child: Baseline(
                  baseline: 40,
                  baselineType: TextBaseline.ideographic,
                  child: Text("adgdvdf"),
                ),
              ),
              Container(
                child: Baseline(
                  baseline: 40,
                  baselineType: TextBaseline.ideographic,
                  child: Text(
                    "111111111111",
                    style: TextStyle(fontSize: 20),
                  ),
                ),
              ),
            ],
          ),
        ),
      ),
    );
  }
}

//const Baseline({
//Key key,
//@required this.baseline,
//@required this.baselineType,
//Widget child,
//})
