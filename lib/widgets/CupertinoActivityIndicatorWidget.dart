import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';

class CupertinoActivityIndicatorWidget extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      home: Scaffold(
        appBar: AppBar(
          title: Text("CupertinoActivityIndicator"),
        ),
        body: Center(
          child: CupertinoActivityIndicator(
            radius: 30.0,
            animating: true,
          ),
        ),
      ),
    );
  }
}


//const CupertinoActivityIndicator({
//Key key,
//this.animating = true,
//this.radius = _kDefaultIndicatorRadius,
//})