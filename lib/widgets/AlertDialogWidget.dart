import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';

class AlertDialogWidget extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return CupertinoPageScaffold(
        child: Container(
      margin: EdgeInsets.all(50),
      child: CupertinoButton(
          child: Text("按钮"),
          onPressed: () {
            showDialog(
              context: context,
              builder: (context) {
                return AlertDialog(
                  title: Text("标题"),
                  titlePadding: EdgeInsets.zero,
                  content: SingleChildScrollView(
                    child: ListBody(
                      children: <Widget>[
                        Text("这是第一行"),
                        Text("这是第一行"),
                        Text("这是第一行"),
                        Text("这是第一行"),
                      ],
                    ),
                  ),
                  contentPadding: EdgeInsets.all(0),
                  actions: <Widget>[
                    FlatButton(
                        onPressed: () {
                          Navigator.of(context).pop();
                        },
                        child: Text("取消")),
                    FlatButton(
                        onPressed: () {
                          Navigator.of(context).pop();
                        },
                        child: Text("确定")),
                  ],
                  actionsPadding: EdgeInsets.all(0),
                  shape: RoundedRectangleBorder(
                      borderRadius: BorderRadius.all(Radius.circular(5))),
                );
              },
            );
          }),
    ));
  }
}

//const AlertDialog({
//Key key,
//this.title,
//this.titlePadding,
//this.titleTextStyle,
//this.content,
//this.contentPadding = const EdgeInsets.fromLTRB(24.0, 20.0, 24.0, 24.0),
//this.contentTextStyle,
//this.actions,
//this.actionsPadding = EdgeInsets.zero,
//this.actionsOverflowDirection,
//this.actionsOverflowButtonSpacing,
//this.buttonPadding,
//this.backgroundColor,
//this.elevation,
//this.semanticLabel,
//this.insetPadding = _defaultInsetPadding,
//this.clipBehavior = Clip.none,
//this.shape,
//this.scrollable = false,
//})
