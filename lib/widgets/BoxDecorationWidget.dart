import 'package:flutter/material.dart';

class BoxDecorationWidget extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      home: Scaffold(
          appBar: AppBar(
            title: Text("BoxDecoration"),
          ),
          body: ListView(
            children: <Widget>[
              Column(
                children: <Widget>[
                  Container(
                    decoration: BoxDecoration(color: Colors.green),
                    width: 100,
                    height: 100,
                    margin: EdgeInsets.only(bottom: 20, left: 20, top: 20),
                    child: Center(
                      child: Text("呵呵"),
                    ),
                  ),
                  Container(
                    decoration: BoxDecoration(color: Colors.green, boxShadow: [
                      BoxShadow(
                          color: Color(0xffff0000),
                          offset: Offset(2.0, 3.0),
                          blurRadius: 6.0)
                    ]),
                    child: Center(
                      child: Text("呵呵"),
                    ),
                    width: 100,
                    height: 100,
                    margin: EdgeInsets.only(bottom: 20, left: 20),
                  ),
                  Container(
                    decoration: BoxDecoration(color: Colors.green, boxShadow: [
                      BoxShadow(
                          color: Color(0xffff0000),
                          offset: Offset(4.0,5.0),
                          blurRadius: 6.0),
                      BoxShadow(
                          color: Color(0xff00ff00),
                          offset: Offset(3.0, 4.0),
                          blurRadius: 3.0),
                      BoxShadow(
                          color: Color(0xff0000ff),
                          offset: Offset(2.0, 3.0),
                          blurRadius: 2.0),
                    ]),
                    child: Center(
                      child: Text("呵呵"),
                    ),
                    width: 100,
                    height: 100,
                    margin: EdgeInsets.only(bottom: 20, left: 20),
                  ),
                  Container(
                    decoration: BoxDecoration(color: Colors.green, boxShadow: [
                      BoxShadow(
                          color: Color(0xffff0000),
                          offset: Offset(2.0, 3.0),
                          spreadRadius: 5.0)
                    ]),
                    child: Center(
                      child: Text("呵呵"),
                    ),
                    width: 100,
                    height: 100,
                    margin: EdgeInsets.only(bottom: 20, left: 20),
                  ),
                  Container(
                    decoration: BoxDecoration(color: Colors.green, boxShadow: [
                      BoxShadow(color: Color(0xffff0000), spreadRadius: 5.0)
                    ]),
                    child: Center(
                      child: Text("呵呵"),
                    ),
                    width: 100,
                    height: 100,
                    margin: EdgeInsets.only(bottom: 20, left: 20),
                  ),
                  Container(
                    decoration: BoxDecoration(
                        border: Border.all(
                            color: Colors.red,
                            width: 1,
                            style: BorderStyle.solid),
                        borderRadius: BorderRadius.all(Radius.circular(5))),
                    width: 100,
                    height: 100,
                    margin: EdgeInsets.only(bottom: 20, left: 20),
                    child: Center(
                      child: Text("呵呵"),
                    ),
                  ),
                  Container(
                    decoration: BoxDecoration(
                        gradient: LinearGradient(colors: [
                      Color(0xff00ff00),
                      Color(0xffffffff),
                      Color(0xff00ff00)
                    ])),
                    width: 100,
                    height: 100,
                    margin: EdgeInsets.only(bottom: 20, left: 20),
                    child: Center(
                      child: Text("呵呵"),
                    ),
                  ),
                ],
              ),
            ],
          )),
    );
  }
}

//const BoxDecoration({
//this.color,
//this.image,
//this.border,
//this.borderRadius,
//this.boxShadow,
//this.gradient,
//this.backgroundBlendMode,
//this.shape = BoxShape.rectangle,
//})
