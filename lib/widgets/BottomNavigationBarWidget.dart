import 'package:flutter/material.dart';

class BottomNavigationBarWidget extends StatefulWidget {
  @override
  State<StatefulWidget> createState() {
    return BottomNavigationBarWidgetState();
  }
}

class BottomNavigationBarWidgetState extends State<BottomNavigationBarWidget> {
  var currentIndex = 0;

  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      home: Scaffold(
        appBar: AppBar(
          title: Text("BottomNavigationBar组件"),
        ),
        body: BottomNavigationBar(
          items: [
            BottomNavigationBarItem(
                icon: Icon(Icons.home),
                title: Text("首页"),
                activeIcon: Icon(
                  Icons.home,
                  color: Colors.red,
                )),
            BottomNavigationBarItem(
                icon: Icon(Icons.alarm),
                title: Text("提醒"),
                activeIcon: Icon(
                  Icons.alarm,
                  color: Colors.red,
                )),
            BottomNavigationBarItem(
                icon: Icon(Icons.mail),
                title: Text("我的"),
                activeIcon: Icon(
                  Icons.mail,
                  color: Colors.red,
                )),
          ],
          selectedItemColor: Colors.red,
          unselectedItemColor: Colors.black,
          selectedFontSize: 15,
          unselectedFontSize: 15,
          currentIndex: currentIndex,
          onTap: (index) {
            setState(() {
              currentIndex = index;
            });
          },
        ),
      ),
    );
  }
}
