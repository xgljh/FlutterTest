import 'package:flutter/material.dart';

class RadioWidget extends StatefulWidget {
  @override
  State<StatefulWidget> createState() {
    return RadioWidgetState();
  }
}

class RadioWidgetState extends State<RadioWidget> {
  String groupValue;

  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      home: Scaffold(
        appBar: AppBar(
          title: Text("Radio组件"),
        ),
        body: Column(
          children: <Widget>[
            Radio(
              value: "aaa",
              groupValue: groupValue,
              onChanged: (result) {
                setState(() {
                  groupValue = result;
                });
              },
              activeColor: Colors.red,
              materialTapTargetSize: MaterialTapTargetSize.padded,
            ),
            Radio(
              value: "bbb",
              groupValue: groupValue,
              onChanged: (result) {
                setState(() {
                  groupValue = result;
                });
              },
              activeColor: Colors.red,
              materialTapTargetSize: MaterialTapTargetSize.padded,
            ),
            RadioListTile(
              value: "ccc",
              groupValue: groupValue,
              onChanged: (result) {
                setState(() {
                  groupValue = result;
                });
              },
              activeColor: Colors.red,
              title: Text("标题"),
              subtitle: Text("副标题副标题副标题副标题副标题副标题副标题副标题副标题"),
              isThreeLine: false,
              secondary: Icon(Icons.alarm),
//                selected:true
              controlAffinity: ListTileControlAffinity.trailing,
            )
          ],
        ),
      ),
    );
  }
}

//const Center({ Key key, double widthFactor, double heightFactor, Widget child })
