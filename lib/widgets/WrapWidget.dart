import 'package:flutter/material.dart';

class WrapWidget extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      home: Scaffold(
        appBar: AppBar(
          title: Text("Wrap组件"),
        ),
        body: Wrap(
          direction: Axis.horizontal,
          alignment: WrapAlignment.end,
          crossAxisAlignment: WrapCrossAlignment.center,
          runAlignment: WrapAlignment.center,
          runSpacing: 50,
          spacing: 10,
          children: <Widget>[
            Chip(
              label: Text("这是一个chip"),
              avatar: Icon(Icons.home),
            ),
            Chip(
              label: Text("这是一个chip"),
              deleteIcon: Icon(Icons.home),
            ),
            Chip(
              label: Text("这是一个chip"),
              avatar: CircleAvatar(
                backgroundImage: NetworkImage(
                    "http://img8.zol.com.cn/bbs/upload/23765/23764201.jpg"),
              ),
            ),
            Chip(
              label: Text("这是一个chip"),
              avatar: Text("avatar"),
            ),
          ],
        ),
      ),
    );
  }
}

//Wrap({
//Key key,
//this.direction = Axis.horizontal,
//this.alignment = WrapAlignment.start,
//this.spacing = 0.0,
//this.runAlignment = WrapAlignment.start,
//this.runSpacing = 0.0,
//this.crossAxisAlignment = WrapCrossAlignment.start,
//this.textDirection,
//this.verticalDirection = VerticalDirection.down,
//List<Widget> children = const <Widget>[],
//})
