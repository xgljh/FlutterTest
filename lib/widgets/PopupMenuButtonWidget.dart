import 'package:flutter/material.dart';

class PopupMenuButtonWidget extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      home: Scaffold(
        appBar: AppBar(
          title: Text("PopupMenuButton组件"),
          actions: <Widget>[
            PopupMenuButton(
              itemBuilder: (context) {
                return <PopupMenuItem<String>>[
                  PopupMenuItem(
                    child: Text("aaaaaa"),
                    value: "111111",
                  ),
                  PopupMenuItem(
                    child: Text("bbbbbb"),
                    value: "222222",
                  )
                ];
              },
//              child: Text("bbbbbb"),

              onSelected: (value) {
                switch (value) {
                  case "111111":
                    print("!!!!!!!!!aaaaaa");
                    break;
                  case "222222":
                    print("!!!!!!!!!bbbbb");
                    break;
                }
              },
              onCanceled: () {
                print("!!!!!!!!!onCanceled");
              },
              initialValue: "222222",
//              offset: Offset(50,50),
              padding: EdgeInsets.only(top: 20),
              color: Colors.red,
            ),
          ],
        ),
      ),
    );
  }
}
//
//const PopupMenuButton({
//Key key,
//@required this.itemBuilder,
//this.initialValue,
//this.onSelected,
//this.onCanceled,
//this.tooltip,
//this.elevation,
//this.padding = const EdgeInsets.all(8.0),
//this.child,
//this.icon,
//this.offset = Offset.zero,
//this.enabled = true,
//this.shape,
//this.color,
//this.captureInheritedThemes = true,
//})
