import 'package:flutter/material.dart';

class OffStageWidget extends StatefulWidget {
  @override
  State<StatefulWidget> createState() {
    return OffStageWidgetState();
  }
}

class OffStageWidgetState extends State<OffStageWidget> {
  var visibility = true;

  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      home: Scaffold(
        body: Container(
          width: 100,
          color: Colors.red,
          child: ListView(
            children: <Widget>[
              RaisedButton(
                onPressed: () {
                  setState(() {
                    visibility = !visibility;
                  });
                },
                child: Text(visibility?"隐藏":"显示"),
              ),
              Offstage(
                offstage: !visibility,
                child: Image.network(
                  "http://img8.zol.com.cn/bbs/upload/23765/23764201.jpg",
                  width: 100,
                  height: 80,
                ),
              ),
            ],
          ),
        ),
      ),
    );
  }
}
