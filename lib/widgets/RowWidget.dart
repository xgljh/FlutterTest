import 'package:flutter/material.dart';

class RowWidget extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      home: Scaffold(
        appBar: AppBar(
          title: Text("appbar"),
        ),
        body: Row(
          children: <Widget>[
            Container(
              width: 60,
              height: 50,
              color: Colors.red,
            ),
            Container(
              width: 100,
              height: 100,
              color: Colors.green,
            ),
            Container(
              width: 100,
              height: 80,
              color: Colors.red,
            ),
            Container(
              width: 80,
              height: 100,
              color: Colors.blue,
            ),
          ],
          mainAxisAlignment: MainAxisAlignment.spaceEvenly,
//          mainAxisSize: MainAxisSize.min,
        crossAxisAlignment: CrossAxisAlignment.end,
        ),
      ),
    );
  }
}


//Row({
//Key key,
//mainAxisAlignment主轴上的对齐方式
//center：将children放置在主轴的中心；
//end：将children放置在主轴的末尾；
//spaceAround：将主轴方向上的空白区域均分，使得children之间的空白区域相等，首尾空白区域为children之间的1/2；
//spaceBetween：将主轴方向上的空白区域均分，使得children之间的空白区域相等，首尾没有空白区域；
//spaceEvenly：将主轴方向上的空白区域均分，使得children之间和收尾的空白区域相等；
//start：将children放置在主轴的起点；

//MainAxisAlignment mainAxisAlignment = MainAxisAlignment.start,
//MainAxisSize mainAxisSize = MainAxisSize.max,
//CrossAxisAlignment crossAxisAlignment = CrossAxisAlignment.center,
//TextDirection textDirection,
//VerticalDirection verticalDirection = VerticalDirection.down,
//TextBaseline textBaseline,
//List<Widget> children = const <Widget>[],
//})