import 'package:flutter/material.dart';

class CardWidget extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      home: Scaffold(
        appBar: AppBar(
          title: Text("Card组件"),
        ),
        body: Container(
//          color: Colors.red,
          decoration: BoxDecoration(
              shape: BoxShape.rectangle,
              border: Border.all(color: Colors.black, width: 1)),
          width: 100,
          height: 100,
          margin: EdgeInsets.all(10),
          child: Card(
            color: Colors.blue,
            elevation: 10,
            shadowColor: Colors.red,
            margin: EdgeInsets.all(20),
            shape:
                RoundedRectangleBorder(borderRadius: BorderRadius.circular(5)),
          ),
        ),
      ),
    );
  }
}

//const Card({
//Key key,
//this.color,
//this.shadowColor,
//this.elevation,
//this.shape,
//this.borderOnForeground = true,
//this.margin,
//this.clipBehavior,
//this.child,
//this.semanticContainer = true,
//})
