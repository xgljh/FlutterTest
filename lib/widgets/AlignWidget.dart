import 'package:flutter/material.dart';

class AlignWidget extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      home: Scaffold(
        appBar: AppBar(
          title: Text("Align"),
        ),
        body: ListView(
          children: <Widget>[
            Container(
              margin: EdgeInsets.only(bottom: 10),
              color: Colors.grey,
              height: 50,
              child: Align(
                child: Text("Alignment.centerLeft"),
                alignment: Alignment.centerLeft,
              ),
            ),
            Container(
              margin: EdgeInsets.only(bottom: 10),
              color: Colors.grey,
              height: 50,
              child: Align(
                child: Text("Alignment.topLeft"),
                alignment: Alignment.topLeft,
              ),
            ),
            Container(
              margin: EdgeInsets.only(bottom: 10),
              color: Colors.grey,
              height: 50,
              child: Align(
                child: Text("Alignment.bottomLeft"),
                alignment: Alignment.bottomLeft,
              ),
            ),

            Container(
              margin: EdgeInsets.only(bottom: 10),
              color: Colors.grey,
              height: 50,
              child: Align(
                child: Text("Alignment.centerRight"),
                alignment: Alignment.centerRight,
              ),
            ),
            Container(
              margin: EdgeInsets.only(bottom: 10),
              color: Colors.grey,
              height: 50,
              child: Align(
                child: Text("Alignment.topRight"),
                alignment: Alignment.topRight,
              ),
            ),
            Container(
              margin: EdgeInsets.only(bottom: 10),
              color: Colors.grey,
              height: 50,
              child: Align(
                child: Text("Alignment.bottomRight"),
                alignment: Alignment.bottomRight,
              ),
            ),

            Container(
              margin: EdgeInsets.only(bottom: 10),
              color: Colors.grey,
              height: 50,
              child: Align(
                child: Text("Alignment.center"),
                alignment: Alignment.center,
              ),
            ),
            Container(
              margin: EdgeInsets.only(bottom: 10),
              color: Colors.grey,
              height: 50,
              child: Align(
                child: Text("Alignment.bottomCenter"),
                alignment: Alignment.bottomCenter,
              ),
            ),
            Container(
              margin: EdgeInsets.only(bottom: 10),
              color: Colors.grey,
              height: 50,
              child: Align(
                child: Text("Alignment.topCenter"),
                alignment: Alignment.topCenter,
              ),
            ),

            Container(
              margin: EdgeInsets.only(bottom: 10),
              color: Colors.grey,
              child: Align(
                child: Container(
                  color: Colors.red,
                  height: 50,
                  child: Text("heightFactor 高是子组件的倍数,Align的高度就是child的高度乘以这个值"),
                ),
                heightFactor: 2,
              ),
            ),

            Container(
              margin: EdgeInsets.only(bottom: 10),
              color: Colors.grey,
              height: 50,
              child: Align(
                child: Container(
                  color: Colors.red,
                  child: Text("Alignment -1.0是左边对齐，1.0是右边对齐"),
                ),
                alignment: Alignment(-1, 0),
              ),
            ),
            Container(
              margin: EdgeInsets.only(bottom: 10),
              color: Colors.grey,
              height: 50,
              child: Align(
                child: Container(
                  color: Colors.red,
                  child: Text("Alignment -1.0是左边对齐，1.0是右边对齐"),
                ),
                alignment: Alignment(0, 0),
              ),
            ),
            Container(
              margin: EdgeInsets.only(bottom: 10),
              color: Colors.grey,
              height: 50,
              child: Align(
                child: Container(
                  color: Colors.red,
                  child: Text("Alignment -1.0是左边对齐，1.0是右边对齐"),
                ),
                alignment: Alignment(1, 0),
              ),
            ),
          ],
        ),
      ),
    );
  }
}


//const Align({
//Key key,
//this.alignment = Alignment.center,
//this.widthFactor,
//this.heightFactor,
//Widget child,
//})