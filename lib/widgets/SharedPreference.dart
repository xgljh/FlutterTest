import 'package:flutter/material.dart';
import 'package:shared_preferences/shared_preferences.dart';

class SharedPreferenceDemo extends StatefulWidget {
  @override
  State<StatefulWidget> createState() {
    return SharedPreferenceDemoState();
  }
}

class SharedPreferenceDemoState extends State<SharedPreferenceDemo> {
  Future<SharedPreferences> _shared = SharedPreferences.getInstance();
  int coun;

  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      home: Scaffold(
        appBar: AppBar(
          title: Text("SharedPreference"),
        ),
        body: Column(
          children: <Widget>[
            RaisedButton(
              onPressed: () async {
                SharedPreferences shared = await _shared;
                int count = (shared.getInt("count") ?? 0) + 1;
                setState(() {
                  shared.setInt("count", count).then((value) => count);
                });
              },
              child: Text("增加"),
            ),
            RaisedButton(
              onPressed: () async {
                SharedPreferences shared = await _shared;
                coun = shared.getInt("count");
              },
              child: Text("获取"),
            ),
          ],
        ),
      ),
    );
  }
}
