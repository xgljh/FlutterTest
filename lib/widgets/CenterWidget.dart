import 'package:flutter/material.dart';

class CenterWidget extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      home: Scaffold(
        appBar: AppBar(
          title: Text("Center组件"),
        ),
        body: Container(
          color: Colors.green,
          child: Center(
            child: Container(
              width: 80,
              height: 60,
              child: Text("widthFactor 子控件width的倍数"),
            ),
            widthFactor: 3,
            heightFactor: 2,
          ),
        ),
      ),
    );
  }
}

//const Center({ Key key, double widthFactor, double heightFactor, Widget child })
