import 'package:flutter/material.dart';

class FittedBoxWidget extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      home: Scaffold(
        appBar: AppBar(
          title: Text("FittedBo组件"),
        ),
        body: SingleChildScrollView(
          child: Column(
            children: <Widget>[
              Container(
                width: 200,
                height: 200,
                margin: EdgeInsets.only(bottom: 10),
                color: Colors.grey,
                child: FittedBox(
                  fit: BoxFit.fill,
                  child: Container(
                    width: 100,
                    height: 100,
                    color: Colors.red,
                  ),
                ),
              ),
            ],
          ),
        ),
      ),
    );
  }
}

//缩放布局，用来给元素做缩放和位置调整,子组件大小将失效
//const FittedBox({
//Key key,
//this.fit = BoxFit.contain,
//this.alignment = Alignment.center,
//Widget child,
//})