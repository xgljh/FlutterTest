import 'package:flutter/material.dart';

class FormWidget extends StatefulWidget {
  @override
  State<StatefulWidget> createState() {
    return FormWidgetState();
  }
}

class FormWidgetState extends State<FormWidget> {
  GlobalKey<FormState> _globalKey = GlobalKey();
  String name;

  void dologin() {
    FormState formState = _globalKey.currentState;
    if (formState.validate()) {
      print("!!!!!!!!!!!!!!!! formState.save()前");
      formState.save();
      print("!!!!!!!!!!!!!!!! formState.save()后");
    }
  }

  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      home: Scaffold(
        appBar: AppBar(
          title: Text("Form"),
        ),
        body: Container(
          padding: EdgeInsets.all(10),
          color: Colors.grey,
          child: Form(
            key: _globalKey,
            child: Column(
              children: <Widget>[
                TextFormField(
                  decoration: InputDecoration(
                      hintText: "请输入用户名",
                      hintStyle: TextStyle(color: Colors.blue, fontSize: 12)),
                  style: TextStyle(color: Colors.red, fontSize: 15),
                  onSaved: (value) {
                    name = value;
                    print("onSaved!!!!!!$value");
                  },
                  onFieldSubmitted: (value) {
                    print("onFieldSubmitted!!!!!!$value");
                  },
                  onChanged: (value) {
                    print("onChanged!!!!!!$value");
                  },
                  onEditingComplete: () {
                    print("onEditingComplete!!!!!!");
                  },
                ),
                Container(
                  margin: EdgeInsets.only(top: 20.0),
                  width: 200,
                  height: 40,
                  child: RaisedButton(
                    onPressed: dologin,
                    child: Text("登录"),
                  ),
                )
              ],
            ),
          ),
        ),
      ),
    );
  }
}
