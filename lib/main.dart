import 'package:flutter/material.dart';
import 'package:flutterapp/pages/FourPage.dart';
import 'package:flutterapp/pages/ThreePage.dart';
import 'pages/MainPage.dart';
import 'pages/OnePage.dart';
import 'pages/TwoPage.dart';

void main() {
  runApp(MaterialApp(
    home: MainPage(),
//    initialRoute: "one", // 默认路由信息
    routes: {
      //全部路由信息
      "one": (context) {
        return OnePage();
      },
      "two": (context) => TwoPage(),
//      "three":(context)=>ThreePage(),
      "four": (context) => FourPage()
    },
    onGenerateRoute: (setting) {
      if (setting.name == ThreePage.routerName) {
        var text = setting.arguments;
        return MaterialPageRoute(builder: (context) {
          return ThreePage(
            text: "hello three",
          );
        });
      }
    },
  ));
}
