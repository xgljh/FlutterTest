import 'package:flutter/material.dart';

class FourPage extends StatefulWidget{
  @override
  State<StatefulWidget> createState() {
    return FourPageState();
  }
}

class FourPageState extends State<FourPage>{
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      home: Scaffold(
        appBar: AppBar(
          title: Text("第四个页面"),
        ),
        body: RaisedButton(
          child: Text("返回，带回数据"),
          onPressed: (){
            Navigator.of(context).pop("我是从第四个页面返回带回来的信息");
          },
        ),
      ),
    );
  }
}