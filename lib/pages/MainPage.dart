import 'package:flutter/material.dart';
import 'package:flutterapp/bean/ItemInfo.dart';
import 'package:flutterapp/widgets/AboutDialogWidget.dart';
import 'package:flutterapp/widgets/AlertDialogWidget.dart';
import 'package:flutterapp/widgets/AlignWidget.dart';
import 'package:flutterapp/widgets/AppBarWidget.dart';
import 'package:flutterapp/widgets/AspectRatioWidget.dart';
import 'package:flutterapp/widgets/BaselineWidget.dart';
import 'package:flutterapp/widgets/BottomNavigationBarWidget.dart';
import 'package:flutterapp/widgets/BottomSheetWidget.dart';
import 'package:flutterapp/widgets/BoxDecorationWidget.dart';
import 'package:flutterapp/widgets/BoxShadowWidget.dart';
import 'package:flutterapp/widgets/CardWidget.dart';
import 'package:flutterapp/widgets/CenterWidget.dart';
import 'package:flutterapp/widgets/ChipWidget.dart';
import 'package:flutterapp/widgets/CircleAvatarWidget.dart';
import 'package:flutterapp/widgets/CircularProgressIndicatorWidget.dart';
import 'package:flutterapp/widgets/ClipOvalWidget.dart';
import 'package:flutterapp/widgets/ClipPathWidget.dart';
import 'package:flutterapp/widgets/ClipRRectWidget.dart';
import 'package:flutterapp/widgets/ClipRectWidget.dart';
import 'package:flutterapp/widgets/ColumnWidget.dart';
import 'package:flutterapp/widgets/CupertinoActivityIndicatorWidget.dart';
import 'package:flutterapp/widgets/CupertinoAlertDialogWidget.dart';
import 'package:flutterapp/widgets/CupertinoButtonWidget.dart';
import 'package:flutterapp/widgets/CupertinoPageScaffoldWidget.dart';
import 'package:flutterapp/widgets/CupertinoTabScaffoldWidget.dart';
import 'package:flutterapp/widgets/DecorationBoxWidget.dart';
import 'package:flutterapp/widgets/DialogWidget.dart';
import 'package:flutterapp/widgets/DismissibleWidget.dart';
import 'package:flutterapp/widgets/DrawerWidget.dart';
import 'package:flutterapp/widgets/ExpandedWidget.dart';
import 'package:flutterapp/widgets/FittedBoxWidget.dart';
import 'package:flutterapp/widgets/FlatButtonWidget.dart';
import 'package:flutterapp/widgets/FloatingActionButtonWidget.dart';
import 'package:flutterapp/widgets/FormWidget.dart';
import 'package:flutterapp/widgets/GridViewWidget.dart';
import 'package:flutterapp/widgets/ImageWidget.dart';
import 'package:flutterapp/widgets/IndexedStackWidget.dart';
import 'package:flutterapp/widgets/LinearProgressIndicatorWidget.dart';
import 'package:flutterapp/widgets/OffStageWidget.dart';
import 'package:flutterapp/widgets/OpacityWidget.dart';
import 'package:flutterapp/widgets/PaddingWidget.dart';
import 'package:flutterapp/widgets/PathProvider.dart';
import 'package:flutterapp/widgets/PopupMenuButtonWidget.dart';
import 'package:flutterapp/widgets/RadioWidget.dart';
import 'package:flutterapp/widgets/RaisedButtonWidget.dart';
import 'package:flutterapp/widgets/RefreshIndicatorWidget.dart';
import 'package:flutterapp/widgets/RotatedBoxWidget.dart';
import 'package:flutterapp/widgets/RowWidget.dart';
import 'package:flutterapp/widgets/ScrollViewWidget.dart';
import 'package:flutterapp/widgets/SimpleDialogWidget.dart';
import 'package:flutterapp/widgets/SizeBoxWidget.dart';
import 'package:flutterapp/widgets/SnackBarWidget.dart';
import 'package:flutterapp/widgets/StackWidget.dart';
import 'package:flutterapp/widgets/SwipeWidget.dart';
import 'package:flutterapp/widgets/SwitchWidget.dart';
import 'package:flutterapp/widgets/TabBarWidget.dart';
import 'package:flutterapp/widgets/TableWidget.dart';
import 'package:flutterapp/widgets/TextFieldWidget.dart';
import 'package:flutterapp/widgets/TextWidget.dart';
import 'package:flutterapp/widgets/WillPopScopeWidget.dart';
import 'package:flutterapp/widgets/WrapWidget.dart';
import 'package:flutterapp/widgets/showModalBottomSheet.dart';

import 'OnePage.dart';

class MainPage extends StatefulWidget {
  @override
  State<StatefulWidget> createState() {
    return MainPageState();
  }
}

class MainPageState extends State<MainPage> {
  var list = [
    ItemInfo(text: "页面跳转"),
    ItemInfo(text: "Text组件"),
    ItemInfo(text: "TextField组件"),
    ItemInfo(text: "TabBar组件"),
    ItemInfo(text: "GridView组件"),
    ItemInfo(text: "RaisedButton组件"),
    ItemInfo(text: "FloatingActionButton组件"),
    ItemInfo(text: "PopupMenuButton组件"),
    ItemInfo(text: "FlatButton组件"),
    ItemInfo(text: "AppBarWidget组件"),
    ItemInfo(text: "BottomNavigationBar组件"),
    ItemInfo(text: "SnackBar组件"),
    ItemInfo(text: "BottomSheet组件"),
    ItemInfo(text: "Align组件"),
    ItemInfo(text: "Center组件"),
    ItemInfo(text: "Padding组件"),
    ItemInfo(text: "Baseline组件"),
    ItemInfo(text: "Chip组件"),
    ItemInfo(text: "Image组件"),
    ItemInfo(text: "CircleAvatar组件"),
    ItemInfo(text: "Drawer组件"),
    ItemInfo(text: "Table组件"),
    ItemInfo(text: "Wrap组件"),
    ItemInfo(text: "SizeBox组件"),
    ItemInfo(text: "AspectRation组件"),
    ItemInfo(text: "DecorationBox组件"),
    ItemInfo(text: "BoxDecoration组件"),
    ItemInfo(text: "ScrollView组件"),
    ItemInfo(text: "BoxShadow组件"),
    ItemInfo(text: "Opacity组件"),
    ItemInfo(text: "LinearProgressIndicator组件"),
    ItemInfo(text: "CircularProgressIndicator组件"),
    ItemInfo(text: "OffState组件"),
    ItemInfo(text: "RefreshIndicator组件"),
    ItemInfo(text: "Card组件"),
    ItemInfo(text: "showModalBottomSheet函数"),
    ItemInfo(text: "Row组件"),
    ItemInfo(text: "Column组件"),
    ItemInfo(text: "Expanded,Flexible组件"),
    ItemInfo(text: "Form组件"),
    ItemInfo(text: "Swipe组件"),
    ItemInfo(text: "FittedBox组件"),
    ItemInfo(text: "Dismissible组件"),
    ItemInfo(text: "RotatedBox组件"),
    ItemInfo(text: "ClipOval组件"),
    ItemInfo(text: "ClipRRect组件"),
    ItemInfo(text: "ClipRect组件"),
    ItemInfo(text: "ClipPath组件"),
    ItemInfo(text: "Stack组件"),
    ItemInfo(text: "IndexedStack组件"),
    ItemInfo(text: "Switch组件"),
    ItemInfo(text: "CupertinoActivityIndicator组件"),
    ItemInfo(text: "CupertinoButton组件"),
    ItemInfo(text: "CupertinoAlertDialog组件"),
    ItemInfo(text: "CupertinoPageScaffold组件"),
    ItemInfo(text: "CupertinoTabScaffold组件"),
    ItemInfo(text: "SimpleDialog组件"),
    ItemInfo(text: "AlertDialog组件"),
    ItemInfo(text: "AboutDialog组件"),
    ItemInfo(text: "自定义Dialog组件"),
    ItemInfo(text: "Radio组件"),
    ItemInfo(text: "PathProvider"),
    ItemInfo(text: "WillPopScope组件"),
  ];
  var listPages = [
    OnePage(),
    TextWidget(),
    TextFieldWidget(),
    TabBarWidget(),
    GridViewWidget(),
    RaisedButtonWidget(),
    FloatingActionButtonWidget(),
    PopupMenuButtonWidget(),
    FlatButtonWidget(),
    AppBarWidget(),
    BottomNavigationBarWidget(),
    SnackBarWidget(),
    BottomSheetWidget(),
    AlignWidget(),
    CenterWidget(),
    PaddingWidget(),
    BaselineWidget(),
    ChipWidget(),
    ImageWidget(),
    CircleAvatarWidget(),
    DrawerWidget(),
    TableWidget(),
    WrapWidget(),
    SizeBoxWidget(),
    AspectRatioWidget(),
    DecorationBoxWidget(),
    BoxDecorationWidget(),
    ScrollViewWidget(),
    BoxShadowWidget(),
    OpacityWidget(),
    LinearProgressIndicatorWidget(),
    CircularProgressIndicatorWidget(),
    OffStageWidget(),
    RefreshIndicatorWidget(),
    CardWidget(),
    ShowModalBottomSheet(),
    RowWidget(),
    ColumnWidget(),
    ExpandedWidget(),
    FormWidget(),
    SwipeWidget(),
    FittedBoxWidget(),
    DismissibleWidget(),
    RotatedBoxWidget(),
    ClipOvalWidget(),
    ClipRRectWidget(),
    ClipRectWidget(),
    ClipPathWidget(),
    StackWidget(),
    IndexedStackWidget(),
    SwitchWidget(),
    CupertinoActivityIndicatorWidget(),
    CupertinoButtonWidget(),
    CupertinoAlertDialogWidget(),
    CupertinoPageScaffoldWidget(),
    CupertinoTabScaffoldWidget(),
    SimpleDialogWidget(),
    AlertDialogWidget(),
    AboutDialogWidget(),
    DialogWidgetTest(),
    RadioWidget(),
    PathProviderDemo(),
    WillPopScopeWidget(),
  ];

  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      home: Scaffold(
        appBar: AppBar(
          title: Text("主页面"),
        ),
        body: ListView.builder(
          itemCount: list.length,
          itemBuilder: (context, position) {
            return ListItem(
                position,
                list[position],
                (position) => {
                      Navigator.of(context).push(new MaterialPageRoute(
                          builder: (BuildContext context) {
                        return listPages[position];
                      }))
                    });
          },
        ),
      ),
    );
  }
}

class ListItem extends StatelessWidget {
  ListItem(this.position, this.item, this.onItemClickListener);

  int position;
  ItemInfo item;
  OnItemClickListener onItemClickListener;

  @override
  Widget build(BuildContext context) {
    return InkWell(
      child: ListTile(
        title: Text(
          item.text,
        ),
      ),
      onTap: () {
        onItemClickListener(position);
      },
    );
  }
}

typedef OnItemClickListener = void Function(int position);
