import 'package:flutter/material.dart';

class TwoPage extends StatefulWidget {
  @override
  State<StatefulWidget> createState() {
    return TwoPageState();
  }
}

class TwoPageState extends State<TwoPage> {
  @override
  Widget build(BuildContext context) {
    var string = ModalRoute.of(context).settings.arguments;

    return MaterialApp(
      home: Scaffold(
        appBar: AppBar(
          title: Text("two 页面"),
        ),
        body: Center(
          child: Column(
            children: <Widget>[
              Text(
                string ?? "",
                style: TextStyle(
                  color: Colors.red,
                  fontSize: 18,
                ),
              ),
              Divider(
                height: 2,
                color: Colors.black,
              ),
              RaisedButton(
                child: Text("返回上个页面"),
                onPressed: () {
                  Navigator.pop(context);
                },
              ),
              RaisedButton(
                child: Text("返回上个页面,传递参数"),
                onPressed: () {
                  Navigator.pop(context);
                },
              )
            ],
          ),
        ),
        floatingActionButton: FloatingActionButton(
          child: Text("跳转"),
          onPressed: () => {},
        ),
      ),
    );
  }
}
