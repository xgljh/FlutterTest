import 'package:flutter/material.dart';

class ThreePage extends StatefulWidget {
  static const routerName = "threepage";

  String text;

  ThreePage({this.text});

  @override
  State<StatefulWidget> createState() {
    return ThreePageState(text: text);
  }
}

class ThreePageState extends State<ThreePage> {
  String text;

  ThreePageState({this.text});

  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      home: Scaffold(
        appBar: AppBar(
          title: Text("第三个页面"),
        ),
        body: Center(
          child: Text("来自第一个页面的数据${text}"),
        ),
      ),
    );
  }
}
