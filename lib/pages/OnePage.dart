import 'package:flutter/material.dart';
import 'package:flutterapp/pages/ThreePage.dart';

class OnePage extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      home: Scaffold(
        appBar: AppBar(
          title: Text("one 页面"),
        ),
        body: Column(
          children: <Widget>[
            InkWell(
              child: Text(
                "不带参数跳转到下个页面",
                style: TextStyle(fontSize: 18),
              ),
              onTap: () {
                Navigator.pushNamed(context, "two");
              },
            ),
            InkWell(
              child: Text("带参数跳转到下个页面，ModalRoute.of(context).settings.arguments", style: TextStyle(fontSize: 18)),
              onTap: () {
                Navigator.pushNamed(context, "two", arguments: "来自第一个页面的数据");
              },
            ),
            InkWell(
              child:
                  Text("带参数跳转到下个页面，onGenerateRoute方式获取", style: TextStyle(fontSize: 18)),
              onTap: () {
                Navigator.pushNamed(context, ThreePage.routerName, arguments: "来自第一个页面的数据")
                    .then((value) {});
              },
            ),
            InkWell(
              child:
                  Text("跳转到下个页面,并接受下个页面返回的参数", style: TextStyle(fontSize: 18)),
              onTap: () {
                Navigator.pushNamed(context, "four", arguments: "来自第一个页面的数据")
                    .then((value) {
                  print("!!!!!!!!!!!"+value);
                });
              },
            ),
          ],
        ),
        floatingActionButton: FloatingActionButton(
          child: Text("跳转"),
          onPressed: () => {Navigator.pushNamed(context, "two")},
        ),
      ),
    );
  }
}
